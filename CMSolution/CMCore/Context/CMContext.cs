﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Model;

namespace CMCore.Context
{
    public class CMContext: DbContext
    {
        public CMContext():base("name=CMContext")
        {
           // DropCreateDatabaseIfModelChanges<CMContext> initializer = new DropCreateDatabaseIfModelChanges<CMContext>();
           // Database.SetInitializer<CMContext>(initializer);
        }

        public DbSet<Secretaria> Secretarias { get; set; }
        public DbSet<Servidor> Servidores { get; set; }
        public DbSet<Prefeitura> Prefeituras { get; set; }
        public DbSet<Horario> Horarios { get; set; }
        public DbSet<Problema> Problemas { get; set; }
        public DbSet<Protocolo> Protocolos { get; set; }
        public DbSet<Procedimento> Procedimentos { get; set; }

        protected override void OnModelCreating(DbModelBuilder mb)
        {
            
            mb.Entity<Protocolo>().Property(x => x.Endereco.Latitude).HasPrecision(10, 8);
            mb.Entity<Protocolo>().Property(x => x.Endereco.Longitude).HasPrecision(11, 8);
            base.OnModelCreating(mb);
        }

        
    }

    
}
