// <auto-generated />
namespace CMCore.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class criaProcedimentos : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(criaProcedimentos));
        
        string IMigrationMetadata.Id
        {
            get { return "201811091648184_criaProcedimentos"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
