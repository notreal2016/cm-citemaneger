namespace CMCore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class criaProcedimentos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Procedimento",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdServidor = c.Long(nullable: false),
                        Datahora = c.DateTime(nullable: false, precision: 0),
                        Descricao = c.String(unicode: false),
                        IdProtocolo = c.Long(nullable: false),
                        Tipo = c.Int(nullable: false),
                        TempoExtra = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Protocolo", t => t.IdProtocolo, cascadeDelete: true)
                .ForeignKey("dbo.Servidor", t => t.IdServidor, cascadeDelete: true)
                .Index(t => t.IdServidor)
                .Index(t => t.IdProtocolo);
            
            CreateTable(
                "dbo.Protocolo",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Identification = c.String(unicode: false),
                        IdProblema = c.Long(nullable: false),
                        Endereco_Logradouro = c.String(unicode: false),
                        Endereco_Numero = c.Int(nullable: false),
                        Endereco_Bairro = c.String(unicode: false),
                        Endereco_Cep = c.String(unicode: false),
                        Endereco_Cidade = c.String(unicode: false),
                        Endereco_Uf = c.String(unicode: false),
                        Endereco_Latitude = c.Single(nullable: false),
                        Endereco_Longitude = c.Single(nullable: false),
                        Telefone = c.String(unicode: false),
                        PontoReferencia = c.String(unicode: false),
                        Email = c.String(unicode: false),
                        Datahora = c.DateTime(nullable: false, precision: 0),
                        ArquivoImg = c.Binary(),
                        PrevConclusao = c.DateTime(nullable: false, precision: 0),
                        Status = c.Int(nullable: false),
                        IdSecResponsavel = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Problema", t => t.IdProblema, cascadeDelete: true)
                .ForeignKey("dbo.Secretaria", t => t.IdSecResponsavel, cascadeDelete: true)
                .Index(t => t.IdProblema)
                .Index(t => t.IdSecResponsavel);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Procedimento", "IdServidor", "dbo.Servidor");
            DropForeignKey("dbo.Protocolo", "IdSecResponsavel", "dbo.Secretaria");
            DropForeignKey("dbo.Procedimento", "IdProtocolo", "dbo.Protocolo");
            DropForeignKey("dbo.Protocolo", "IdProblema", "dbo.Problema");
            DropIndex("dbo.Protocolo", new[] { "IdSecResponsavel" });
            DropIndex("dbo.Protocolo", new[] { "IdProblema" });
            DropIndex("dbo.Procedimento", new[] { "IdProtocolo" });
            DropIndex("dbo.Procedimento", new[] { "IdServidor" });
            DropTable("dbo.Protocolo");
            DropTable("dbo.Procedimento");
        }
    }
}
