namespace CMCore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Horario",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Descrição = c.String(unicode: false),
                        Abertura = c.DateTime(nullable: false, precision: 0),
                        Fechamento = c.DateTime(nullable: false, precision: 0),
                        InicioIntervalo = c.DateTime(nullable: false, precision: 0),
                        FimDoIntervalo = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Prefeitura",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nome = c.String(unicode: false),
                        Endereco_Logradouro = c.String(unicode: false),
                        Endereco_Numero = c.Int(nullable: false),
                        Endereco_Bairro = c.String(unicode: false),
                        Endereco_Cep = c.String(unicode: false),
                        Endereco_Cidade = c.String(unicode: false),
                        Endereco_Uf = c.String(unicode: false),
                        Endereco_Latitude = c.Single(nullable: false),
                        Endereco_Longitude = c.Single(nullable: false),
                        IdPrefeito = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Servidor", t => t.IdPrefeito, cascadeDelete: true)
                .Index(t => t.IdPrefeito);
            
            CreateTable(
                "dbo.Servidor",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Login = c.String(unicode: false),
                        Senha = c.String(unicode: false),
                        Nome = c.String(unicode: false),
                        Email = c.String(unicode: false),
                        Celular = c.String(unicode: false),
                        Bloqueado = c.Boolean(nullable: false),
                        Tipo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Secretaria",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nome = c.String(unicode: false),
                        Endereco_Logradouro = c.String(unicode: false),
                        Endereco_Numero = c.Int(nullable: false),
                        Endereco_Bairro = c.String(unicode: false),
                        Endereco_Cep = c.String(unicode: false),
                        Endereco_Cidade = c.String(unicode: false),
                        Endereco_Uf = c.String(unicode: false),
                        Endereco_Latitude = c.Single(nullable: false),
                        Endereco_Longitude = c.Single(nullable: false),
                        Telefone = c.String(unicode: false),
                        Email = c.String(unicode: false),
                        IdSecretario = c.Long(nullable: false),
                        IdHorario = c.Long(nullable: false),
                        IdPrefeitura = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Horario", t => t.IdHorario, cascadeDelete: true)
                .ForeignKey("dbo.Prefeitura", t => t.IdPrefeitura, cascadeDelete: true)
                .ForeignKey("dbo.Servidor", t => t.IdSecretario, cascadeDelete: true)
                .Index(t => t.IdSecretario)
                .Index(t => t.IdHorario)
                .Index(t => t.IdPrefeitura);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Secretaria", "IdSecretario", "dbo.Servidor");
            DropForeignKey("dbo.Secretaria", "IdPrefeitura", "dbo.Prefeitura");
            DropForeignKey("dbo.Secretaria", "IdHorario", "dbo.Horario");
            DropForeignKey("dbo.Prefeitura", "IdPrefeito", "dbo.Servidor");
            DropIndex("dbo.Secretaria", new[] { "IdPrefeitura" });
            DropIndex("dbo.Secretaria", new[] { "IdHorario" });
            DropIndex("dbo.Secretaria", new[] { "IdSecretario" });
            DropIndex("dbo.Prefeitura", new[] { "IdPrefeito" });
            DropTable("dbo.Secretaria");
            DropTable("dbo.Servidor");
            DropTable("dbo.Prefeitura");
            DropTable("dbo.Horario");
        }
    }
}
