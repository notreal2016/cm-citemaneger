namespace CMCore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class entityProblemas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Problema",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Tipo = c.Int(nullable: false),
                        IdSecretaria = c.Long(nullable: false),
                        Descrição = c.String(unicode: false),
                        TempoConclusao = c.DateTime(nullable: false, precision: 0),
                        Corrido = c.Boolean(nullable: false),
                        Ativo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Servidor", "Problema_Id", c => c.Long());
            CreateIndex("dbo.Servidor", "Problema_Id");
            AddForeignKey("dbo.Servidor", "Problema_Id", "dbo.Problema", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Servidor", "Problema_Id", "dbo.Problema");
            DropIndex("dbo.Servidor", new[] { "Problema_Id" });
            DropColumn("dbo.Servidor", "Problema_Id");
            DropTable("dbo.Problema");
        }
    }
}
