﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Model;
using Newtonsoft.Json;

namespace CMCore.Componentes
{
    [Serializable, JsonObject]
    public class StatusProtocolo
    {
        public String Identification { get; set; }
        public DateTime Datahora { get; set; }
        public TipoProcedimento Tipo { get; set; }
    }
}
