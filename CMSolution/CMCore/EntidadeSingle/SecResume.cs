﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CMCore.Model;

namespace CMCore.EntidadeSingle
{
    /// <summary>
    /// Classe encarregada de repassar dados simples da secretaria 
    /// </summary>
    [Serializable, JsonObject]
    public class SecResume
    {
        private Int64 _id;
        private String _nome;
        private List<Protocolo> _protocolos;

        public SecResume(long id, string nome, List<Protocolo> protocolos)
        {
            _id = id;
            _nome = nome;
            _protocolos = protocolos;
        }

        public long Id => _id;

        public string Nome => _nome;

        public List<Protocolo> Protocolos => _protocolos;
    }
}
