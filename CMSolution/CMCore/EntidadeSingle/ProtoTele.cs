﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CMCore.EntidadeSingle
{
    [Serializable, JsonObject]
    public class ProtoTele
    {
        private String _telefone, _identification, _email;

        public ProtoTele(string telefone, string identification, string email = null)
        {
            _telefone = telefone;
            _identification = identification;
            _email = email;
        }

        public string Telefone => _telefone;

        public string Identification => _identification;

        public string Email => _email;
    }
}
