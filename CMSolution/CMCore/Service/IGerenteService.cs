﻿using System.Collections.Generic;
using CMCore.Model;
using Newtonsoft.Json.Linq;

namespace CMCore.Service
{
    public interface IGerenteService
    {
        /// <summary>
        /// Classe encarregada de validar o login de um usuário ao sistema de gerenciamento
        /// </summary>
        /// <param name="login">Login do usuário</param>
        /// <param name="senha">Senha do usuário</param>
        /// <returns>Dados do processo de logar</returns>
        JObject Logar(string login, string senha);

        /// <summary>
        /// Busca por todos os protocolos cujo a secretaria do id do servior repassado tenha vinculo, podendo haver protocolos de mais de uma secretaria
        /// </summary>
        /// <param name="idServidor">Id Servidor</param>
        /// <returns>Lista de Protocolos</returns>
        List<Protocolo> ProtocolosByIdServidor(long idServidor);

        Servidor SalvaServidor(Servidor servidor);
    }
}