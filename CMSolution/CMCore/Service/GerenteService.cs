﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Interfaces;
using CMCore.Model;
using CMCore.Repository;
using CMRestFull.Class;
using Newtonsoft.Json.Linq;

namespace CMCore.Service
{
    /// <summary>
    /// Classe encarregada das gegras de negocios do sistema de gerente
    /// </summary>
    public class GerenteService : IGerenteService
    {
        private IServidorRepository _sr;
        private IProtocoloRepository _pr;
        public GerenteService(IServidorRepository sr, IProtocoloRepository pr)
        {
            _sr = sr;
            _pr = pr;
        }
        /// <summary>
        /// Classe encarregada de validar o login de um usuário ao sistema de gerenciamento
        /// </summary>
        /// <param name="login">Login do usuário</param>
        /// <param name="senha">Senha do usuário</param>
        /// <returns>Dados do processo de logar</returns>
        public JObject Logar(string login, string senha)
        {
            try
            {
                JObject obj = new JObject();
                if (ValidaDados.ExistCaracterEspeciais(login) || senha.Trim().Equals(""))
                {
                    obj.Add("Cod", 501);
                    obj.Add("Mensagem", "Login ou senha inválidos");
                }
                else
                {
                    Servidor servidor = _sr.Logar(login, senha);
                    if (servidor.Bloqueado)
                    {
                        obj.Add("Cod", 501);
                        obj.Add("Mensagem", "Servidor está com acesso bloqueado. Procure o setor competente para maiores informações.");
                    }
                    else
                    {
                        List<Int64[]> sediado = _sr.FindSediado(servidor.Id);
                        obj.Add("Cod", 100);
                        obj.Add("Mensagem", "Login com sucesso");
                        obj.Add("Servidor", JObject.FromObject(servidor));
                        List<JObject> locados = new List<JObject>();
                        sediado.ForEach(s =>
                        {
                            JObject o1= new JObject();
                            o1.Add("IdSecretaria", s[0]);
                            o1.Add("IdPrefeitura", s[1]);
                            locados.Add(o1);
                        });
                        obj.Add("locado", JToken.FromObject(locados));
                    }
                }

                return obj;
            }
            catch (Exception e)
            {
                JObject obj = new JObject();
                obj.Add("Cod", 500);
                obj.Add("Mensagem", String.Format("Erro ao logar: {0} - {1}", e.HResult, e.Message));
                return obj;
            }
        }
        /// <summary>
        /// Busca por todos os protocolos cujo a secretaria do id do servior repassado tenha vinculo, podendo haver protocolos de mais de uma secretaria
        /// </summary>
        /// <param name="idServidor">Id Servidor</param>
        /// <returns>Lista de Protocolos</returns>
        public List<Protocolo> ProtocolosByIdServidor(long idServidor)
        {
            return _pr.FindProtocoloByIdServidor(idServidor) ;
        }

        public Servidor SalvaServidor(Servidor servidor)
        {
            return _sr.Save(servidor);
        }
    }
}
