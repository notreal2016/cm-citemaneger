﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CMCore.Service
{
    public class ServiceSMS
    {
        private AutoResetEvent receiveNow;

        public void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                if (e.EventType == SerialData.Chars)
                {
                    receiveNow.Set();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool EnviarSMS(String numero, String mensagem)
        {
            using (SerialPort serial = new SerialPort())
            {
                bool respota = false;
                try
                {
                    receiveNow = new AutoResetEvent(false);
                    serial.PortName= "COM18";//Nome da porta serial
                    serial.Handshake = Handshake.RequestToSend;
                    serial.DtrEnable = true;
                    serial.RtsEnable = true;
                    serial.NewLine = Environment.NewLine;
                    serial.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                    serial.Open();
                    string recievedData = ExecCommand(serial, "AT", 300, "Telefone não conectado");
                    recievedData = ExecCommand(serial, "AT+CMGF=1", 300, "Falha no formato da mensagem ");
                    string command = "AT+CMGS=\"" + numero + ",145";
                    recievedData = ExecCommand(serial, command, 300, "Falha no número");
                    command = mensagem + char.ConvertFromUtf32(26) + "\r";
                    recievedData = ExecCommand(serial, command, 3000, "Falhaao enviar mensagem");
                    serial.Close();
                    if (recievedData.EndsWith("\r\nOK\r\n"))
                    {
                        respota = true;
                    }
                    else if (recievedData.EndsWith("ERROR"))
                    {
                        respota = false;
                    }
                }
                catch (SystemException e)
                {
                    Console.WriteLine(e);
                    
                }

                return respota;
            }
        }

        private string ExecCommand(SerialPort port, string command, int responseTimeout, string errorMessage)
        {
            try
            {

                port.DiscardOutBuffer();
                port.DiscardInBuffer();
                receiveNow.Reset();
                port.Write(command + "\r");

                string input = ReadResponse(port, responseTimeout);
                if ((input.Length == 0) || ((!input.EndsWith("\r\n> ")) && (!input.EndsWith("\r\nOK\r\n"))))
                    throw new ApplicationException("No success message was received.");
                return input;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ReadResponse(SerialPort port, int timeout)
        {
            string buffer = string.Empty;
            try
            {
                do
                {
                    if (receiveNow.WaitOne(timeout, false))
                    {
                        string t = port.ReadExisting();
                        buffer += t;
                    }
                    else
                    {
                        if (buffer.Length > 0)
                            throw new ApplicationException("Response received is incomplete.");
                        else
                            throw new ApplicationException("No data received from phone.");
                    }
                }
                while (!buffer.EndsWith("\r\nOK\r\n") && !buffer.EndsWith("\r\n> ") && !buffer.EndsWith("\r\nERROR\r\n"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return buffer;
        }
    }
}
