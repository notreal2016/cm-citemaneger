﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Componentes;
using CMCore.Context;
using CMCore.Interfaces;
using CMCore.Model;
using CMCore.Repository;
using CMCore.EntidadeSingle;

namespace CMCore.Service
{
    /// <summary>
    /// Classe encarregada de controlar os serviços voltados ao processo de protocolos do CM
    /// </summary>
    public class ProtocoloService : IProtocoloService
    {
        private IProtocoloRepository pr;
        private ISecretariaRepository sr;
        public ProtocoloService(IProtocoloRepository pr, ISecretariaRepository sr)
        {
            this.pr = pr;
            this.sr = sr;
        }
        /// <summary>
        /// Busca por todos os procedimentos de um determinado protocolo repassado por parametro
        /// </summary>
        /// <param name="phone">Telefone celular vinculado ao protocolo</param>
        /// <param name="identification">Protocolo</param>
        /// <returns>Lista de procedimentos localizados</returns>
        public List<Procedimento> FindProcedimentosbyProtocol(String phone, String identification)
        {
            return pr.FindProcedimentosbyProtocol(phone, identification);
        }

        /// <summary>
        /// Salva os dados de um protocolo
        /// </summary>
        /// <param name="protocolo">Protocolo cujos dados serão salvos</param>
        /// <returns>Protocolo após ser salvo</returns>
        public Protocolo Save(Protocolo protocolo)
        {
            return pr.Save(protocolo);
        }

        /// <summary>
        /// Busca um protocolo pelo Idprotocolo e email do cidadão
        /// </summary>
        /// <param name="IdProtocolo">Id do protocolo enviado ao cidadão</param>
        /// <param name="email">Email do cidadão</param>
        /// <returns>Protocolo referenciado ou null</returns>
        public Protocolo FindByIDAndEmail(String IdProtocolo, String email)
        {
            return pr.FindByIndentificatioEmail(email, IdProtocolo);
        }
        /// <summary>
        /// Busca um protocolo pelo IdProtocolo e Telefone celilar do cidadão
        /// </summary>
        /// <param name="IdProtocolo">Id Protocolo</param>
        /// <param name="phone">Telfone do cidadãos</param>
        /// <returns>Protocolo referenciado ou null</returns>
        public Protocolo FindByIdAndPhone(String IdProtocolo, String phone)
        {
            return pr.FindByIndentificatioPhone(phone, IdProtocolo);
        }
        /// <summary>
        /// Lista todos os Status em ordem cronologica dos processos vinculados a um protocolo
        /// Atendendo ao RF1
        /// </summary>
        /// <param name="idProtocolo">id protocolos</param>
        /// <returns>Lista de status</returns>
        public List<StatusProtocolo> Status(Int64 idProtocolo)
        {
            Protocolo protocolo = pr.FindById(idProtocolo);
            List<StatusProtocolo> result = new List<StatusProtocolo>();
            if (protocolo != null)
            {
                protocolo.Procedimentos.ForEach(p => result.Add(new StatusProtocolo()
                {
                    Tipo = p.Tipo,
                    Identification = protocolo.Identification,
                    Datahora = p.Datahora
                }));
            }
            return result.OrderBy(p1 => p1.Datahora).ToList();
        }

        /// <summary>
        /// Busca por problemas apartir do filtro repassado para serem usados na abertura de protocolo.
        /// </summary>
        /// <param name="tipo">Tipo de problema</param>
        /// <returns>Lista de problemas </returns>
        public List<Problema> ListProblemasByTipo(TipoProblema tipo, CMContext context)
        {
            return sr.FindByTipoProblema(tipo, context);
        }
        /// <summary>
        /// Busca por um problema a partir do Id Repassado
        /// </summary>
        /// <param name="id">Id do problema</param>
        /// <returns>Problema localizado ou null</returns>
        public Problema FindProblemaById(Int64 id)
        {
            return sr.FindProblemaById(id);
        }
        /// <summary>
        /// Busca pelos protocolos ainda não totificados, repassandos só os dados basicos, protocolo e telefone
        /// </summary>
        /// <returns>Lista de proto tele</returns>
        public List<ProtoTele> UpNotAndFindNotNotification(String[] notifications = null)
        {
            if (notifications != null)
            {
                pr.ConfirmaEnvioNotificacao(notifications);
            }
            List<ProtoTele> lista = new List<ProtoTele>();
            var result = pr.FindAllNotNotification();
            result.ForEach(p => lista.Add(new ProtoTele(p.Telefone, p.Identification)));
            return lista;
        }
        /// <summary>
        /// Busca por um protocolo pelo identification repassado
        /// </summary>
        /// <param name="identification">Identification do protocolo a ser buscado</param>
        /// <returns>Protocolo localizado ou null</returns>
        public Protocolo FindByIdentification(String identification)
        {
            return pr.FindyByIdentification(identification);
        }
        
    }
}
