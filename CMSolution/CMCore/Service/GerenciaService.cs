﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.EntidadeSingle;
using CMCore.Interfaces;
using CMCore.Repository;
using CMCore.Model;
using Newtonsoft.Json.Linq;

namespace CMCore.Service
{
   
    /// <summary>
    /// Classe enacrregada das regras de negocio de gerenciamento do processo
    /// </summary>
    public class GerenciaService : IGerenciaService
    {
        private IServidorRepository _sr;
        private ISecretariaRepository _secr;
        private IProtocoloService _ps;
        private IPrefeituraRepository _prr;

        public GerenciaService(IServidorRepository sr, ISecretariaRepository secr, IProtocoloService ps, IPrefeituraRepository prr)
        {
            _sr = sr;
            _secr = secr;
            _prr = prr;
            _ps = ps;
        }
        /// <summary>
        /// Efetua o login do servidor ao sistema 
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="senha">Senha</param>
        /// <returns>Servidor Logado</returns>
        public Servidor Logar(string login, string senha)
        {
            return _sr.Logar(login, senha);
        }

        /// <summary>
        /// Busca Lista de Secretarias e seus protocolos abertos 
        /// </summary>
        /// <returns>Lista de secretarias</returns>
        public List<SecResume> FindResumeSecretariaByServidor(Int64 id)
        {
            var lista = _secr.FindByIdServidor(id);
            List<SecResume> secs = new List<SecResume>();
            if (lista.Count > 0)
            {
                foreach (Secretaria secretaria in lista)
                {
                    secs.Add(new SecResume(secretaria.Id, secretaria.Nome, _ps.FindProtocolosPendentesBySec(secretaria.Id)));
                }
            }
            return secs;
        }
        /// <summary>
        /// Adiciona um servidor ao sistema 
        /// </summary>
        /// <param name="servidor">Servidor cujos dados serão adicionados</param>
        /// <returns>Servidor</returns>
        public Servidor AddServidor(Servidor servidor)
        {
            return _sr.Save(servidor);
        }

        /// <summary>
        /// Salva os dados de um prefeitura 
        /// </summary>
        /// <param name="prefeitura">Prefeitura cujos dados serão salvos</param>
        /// <returns>Prefitura com os dados salvos</returns>
        public Prefeitura SavePrefeitura(Prefeitura prefeitura)
        {
            return _prr.Save(prefeitura);
        }
        /// <summary>
        /// Salva os dados de uma secretaria no banco de dados 
        /// </summary>
        /// <param name="secretaria">Secretaria cujos dados serão salvos</param>
        /// <returns>Secretaria</returns>
        public Secretaria SaveSecretaria(Secretaria secretaria)
        {
            return _secr.Save(secretaria);
        }
        /// <summary>
        /// Busca por um servidor pelo id repassado 
        /// </summary>
        /// <param name="id">Id do servidor</param>
        /// <returns>Servidor ou null</returns>
        public Servidor FidServidorById(long id)
        {
            return _sr.FindById(id);
        }

        /// <summary>
        /// Vincula um servidor a uma secretaria pelos ides repassados
        /// </summary>
        /// <param name="idServidor">Id do servidor</param>
        /// <param name="idSecretaria">Id da secretaria</param>
        public void VinculaServidor(long idServidor, long idSecretaria)
        {
            if (idSecretaria <= 0 || idServidor <= 0)
            {
                throw new Exception("Id do Servidor ou da Secretaria inválidos");
            }

            Servidor ser = _sr.FindById(idServidor);
            if ( ser == null)
            {
                throw new Exception("Servidor não localizado.");
            }

            Secretaria sec = _secr.FindById(idSecretaria);
            if ( sec == null)
            {
                throw new Exception("Secretaria não localizada.");
            }

            sec.VinculaServidor(ser);
            _secr.Save(sec);
        }

        /// <summary>
        /// Busca por todas as secretarias de uma determinada prefeitura
        /// </summary>
        /// <param name="idPrefeitura">Id da prefeitura</param>
        /// <returns>Lista de JObjetc contendo id e nome das secretarias</returns>
        public List<JObject> GetAllSecretarias(long idPrefeitura)
        {
            var secs = _secr.AllSecretariasByPrefeitura(idPrefeitura);
            List<JObject> result = new List<JObject>();
            secs.ForEach(s =>
            {
                JObject j = new JObject();
                j.Add("Id", s.Id);
                j.Add("Nome", s.Nome.ToUpper());
                result.Add(j);
            });
            return result;
        }
    }

}
