﻿using System;
using System.Runtime.Serialization;

namespace CMCore.Repository
{
    [Serializable]
    internal class ServidorException : Exception
    {
        public ServidorException()
        {
        }

        public ServidorException(string message) : base(message)
        {
        }

        public ServidorException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ServidorException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}