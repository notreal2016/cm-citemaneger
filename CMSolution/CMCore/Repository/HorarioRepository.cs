﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Context;
using CMCore.Model;

namespace CMCore.Repository
{
    /// <summary>
    /// Classe encarregada do processo de peristência de dados de Horarios no banco de dados 
    /// </summary>
    public class HorarioRepository
    {
        private CMContext _context;

        public HorarioRepository(CMContext context)
        {
            this._context = context;
        }
        /// <summary>
        /// Persiste os dados de uma horario o banco de dados 
        /// </summary>
        /// <param name="horario">Horaio cujos dados serão persistidos</param>
        /// <returns>Horairo após a persistencia de dados</returns>
        public Horario Save(Horario horario)
        {
            using (var transacao = _context.Database.BeginTransaction())
            {
                try
                {
                    if (horario.Id == 0)
                    {
                        _context.Horarios.Add(horario);
                    }
                    else
                    {
                        _context.Entry(horario).State = EntityState.Modified;
                    }

                    _context.SaveChanges();
                    transacao.Commit();
                }
                catch (Exception e)
                {
                    transacao.Rollback();
                    throw new HorarioException(e.Message);
                }
            }
            return horario;
        }

        public Horario FindById(Int64 id)
        {
            return _context.Horarios.Find(id);
        }

        /// <summary>
        /// Remove um horario do banco de dados 
        /// </summary>
        /// <param name="id"></param>
        public void Remove(Int64 id)
        {
            using (var transacao = _context.Database.BeginTransaction())
            {
                try
                {
                    Horario h = FindById(id);
                    _context.Horarios.Remove(h);
                    _context.SaveChanges();
                    transacao.Commit();
                }
                catch (Exception e)
                {
                    transacao.Rollback();
                    throw new HorarioException(e.Message);
                }
            }
        }
        /// <summary>
        /// Busca por todos os horarios cadastrados no banco de dados
        /// </summary>
        /// <returns>Lista de todos os horarios cadastrados</returns>
        public List<Horario> FindAll()
        {
            return _context.Horarios.ToList();
        }
    }
}
