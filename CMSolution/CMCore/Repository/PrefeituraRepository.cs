﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Context;
using CMCore.Model;

namespace CMCore.Repository
{
    public class PrefeituraRepository
    {
        private CMContext _context;

        public PrefeituraRepository(CMContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Persiste os dados de um prefeitura no banco de dados
        /// </summary>
        /// <param name="prefeitura">Dados da prefeitura que serão persistidos</param>
        /// <returns>Objeto prefeitura após a persistência</returns>
        public Prefeitura Save(Prefeitura prefeitura)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (prefeitura.Id == 0)
                    {
                        _context.Prefeituras.Add(prefeitura);
                    }
                    else
                    {
                        _context.Entry(prefeitura).State = EntityState.Modified;
                    }

                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw new PrefeituraException(e.Message);
                }
            }

            return prefeitura;
        }

        /// <summary>
        /// Busca uma prefeitura apartir do id repassado
        /// </summary>
        /// <param name="idPrefeitura">Id da prefeitura</param>
        /// <returns>Prefeitura localizada ou Null quando não localizado</returns>
        public Prefeitura FindById(Int64 idPrefeitura)
        {
            return _context.Prefeituras.Find(idPrefeitura);
        }
        /// <summary>
        /// Remove os dados de uma prefeitura do banco de dados
        /// </summary>
        /// <param name="id">Id da refeitura</param>
        public void Remove(Int64 id)
        {
            using (var transacion = _context.Database.BeginTransaction())
            {
                try
                {
                    Prefeitura p = FindById(id);
                    _context.Prefeituras.Remove(p);
                    _context.SaveChanges();
                    transacion.Commit();
                }
                catch (Exception e)
                {
                    transacion.Rollback();
                    throw new PrefeituraException(e.Message);
                }
            }
        }

    }
}
