﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Context;
using CMCore.Model;

namespace CMCore.Repository
{
    public class SecretariaRepository: ISecretariaRepository
    {
        private CMContext _context ;

        public SecretariaRepository(CMContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Persiste os dados de uma secretaria no banco de dados 
        /// </summary>
        /// <param name="secretaria">Dados da secretaria que será persistido no banco de dados</param>
        /// <returns>Secretaria após dados persistidos</returns>
        public Secretaria Save(Secretaria secretaria)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (secretaria.Id == 0)
                    {
                        _context.Secretarias.Add(secretaria);

                    }
                    else
                    {
                        _context.Entry(secretaria).State = EntityState.Modified;
                    }

                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw new SecretariaException(e.Message);
                }
            }

            return secretaria;
        }
        /// <summary>
        /// Busca por uma secretaria apartir do id Repassado
        /// </summary>
        /// <param name="id">Id da secretaria</param>
        /// <returns>Objeto Secretaria ou null caso não seja encontrado</returns>
        public Secretaria FindById(Int64 id)
        {
            return _context.Secretarias.Find(id);
        }

        /// <summary>
        /// Busca por todas as secretarias de uma prefeitura
        /// </summary>
        /// <param name="idPrefeitura">Id da prefeitura em questão</param>
        /// <returns>Lista de secretarias cadastradas</returns>
        public List<Secretaria> AllSecretariasByPrefeitura(Int64 idPrefeitura)
        {
            var result = _context.Secretarias.Where(s => s.IdPrefeitura == idPrefeitura)
                .OrderBy(s => s.Nome).ToList();
            return result;
        }
        /// <summary>
        /// Remove uma secretária apartir do Id repassado
        /// </summary>
        /// <param name="id">Id da secretaria removida.</param>
        public void Remove(Int64 id)
        {
            using (var transacao = _context.Database.BeginTransaction())
            {
                try
                {
                    Secretaria s = FindById(id);
                    _context.Secretarias.Remove(s);
                    _context.SaveChanges();
                    transacao.Commit();
                }
                catch (Exception e)
                {
                    transacao.Rollback();
                    throw  new SecretariaException(e.Message);
                }
                
            }
        }
        /// <summary>
        /// Lista todos os problemas cadastros pelo tipo repassado como paramentro
        /// </summary>
        /// <param name="tipo">Tipo de problema a ser filtrado</param>
        /// <returns>Lista de problemas cadastrados</returns>
        public List<Problema> FindByTipoProblema(TipoProblema tipo, CMContext context)
        {
            List<Problema> lista = new List<Problema>();
            _context.Secretarias.ForEachAsync(s =>
                s.Problemas.Where(p1 => p1.Tipo == tipo).ToList()
                    .ForEach(r => lista.Add(r)));
            return lista;
        }
        /// <summary>
        /// Busca por um problema a partir do Id repassado
        /// </summary>
        /// <param name="id">Id do problema a ser localizado</param>
        /// <returns>Retorna o Problema localizado ou null </returns>
        public Problema FindProblemaById(long id)
        {
            return _context.Problemas.Find(id);
            
        }
    }
}
