﻿using System;
using System.Runtime.Serialization;

namespace CMCore.Repository
{
    [Serializable]
    internal class ProtocoloException : Exception
    {
        public ProtocoloException()
        {
        }

        public ProtocoloException(string message) : base(message)
        {
        }

        public ProtocoloException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ProtocoloException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}