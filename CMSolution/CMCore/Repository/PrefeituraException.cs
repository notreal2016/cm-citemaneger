﻿using System;
using System.Runtime.Serialization;

namespace CMCore.Repository
{
    [Serializable]
    internal class PrefeituraException : Exception
    {
        public PrefeituraException()
        {
        }

        public PrefeituraException(string message) : base(message)
        {
        }

        public PrefeituraException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PrefeituraException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}