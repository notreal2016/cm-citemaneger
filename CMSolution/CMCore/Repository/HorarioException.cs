﻿using System;
using System.Runtime.Serialization;

namespace CMCore.Repository
{
    [Serializable]
    internal class HorarioException : Exception
    {
        public HorarioException()
        {
        }

        public HorarioException(string message) : base(message)
        {
        }

        public HorarioException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected HorarioException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}