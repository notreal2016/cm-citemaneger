﻿using System;
using System.Runtime.Serialization;

namespace CMCore.Repository
{
    [Serializable]
    internal class SecretariaException : Exception
    {
        public SecretariaException()
        {
        }

        public SecretariaException(string message) : base(message)
        {
        }

        public SecretariaException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SecretariaException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}