﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CMCore.Context;
using CMCore.Model;

namespace CMCore.Repository
{
    public class ServidorRepository : IServidorRepository
    {
        private CMContext _context;

        public ServidorRepository(CMContext context)
        {
            _context = context;
        }

        private Servidor Update(Servidor servidor)
        {
            
                using (var transacao = _context.Database.BeginTransaction())
                {
                    try
                    {
                        if (servidor.Id == 0)
                        {
                            _context.Servidores.Add(servidor);
                        }
                        else
                        {
                            _context.Entry(servidor).State = EntityState.Modified;
                        }
                        
                        _context.SaveChanges();
                        transacao.Commit();
                        return servidor;
                    }
                    catch (Exception e)
                    {
                        transacao.Rollback();
                        throw new ServidorException(e.Message);
                    }        
                }
            
        }
        /// <summary>
        /// Persiste os dados de um servidor no banco de dados
        /// </summary>
        /// <param name="servidor">Servidor cujo os dados serão persistidos</param>
        /// <returns>Servidor ápos a persistencia dos dados</returns>
        public Servidor Save(Servidor servidor)
        {
           return Update(servidor);
        }
        /// <summary>
        /// Busca por um servidor apartir do id repassado
        /// </summary>
        /// <param name="id">Id do servidor</param>
        /// <returns>Servidor localizado ou null caso não seja localizado</returns>
        public Servidor FindById(Int64 id)
        {
            return _context.Servidores.Find(id);
        }
        /// <summary>
        /// Busca todos os servidores de uma secretaria em ordem alfabetica
        /// </summary>
        /// <param name="idSecretaria">Id da secretaria a ser pesquisada</param>
        /// <returns>Lista de servidores</returns>
        public List<Servidor> FindAllServidoresBySecretaria(Int64 idSecretaria)
        {
            var result = _context.Servidores.SqlQuery("Select * from servidor where idSecretaria =" + idSecretaria);
            return result.OrderBy(s => s.Nome).ToList();
        }
        /// <summary>
        /// Busca todos os servidores vinculados a uma prefeitura
        /// </summary>
        /// <param name="idPrefeitura">Id da prefeitura repassada como paramentro</param>
        /// <returns>Lista de todos os servidores da prefeitura informada pelo id.</returns>
        public List<Servidor> FindAllServidoresByPrefeitura(Int64 idPrefeitura)
        {
            var result = _context.Servidores.SqlQuery("Select servidor.* from servidor left join secrtaria on servidor.idsecretaria = secretaria.id where secretaria.idPrefeitura =" + idPrefeitura);
            return result.OrderBy(s => s.Nome).ToList();
        }
        /// <summary>
        /// Remove um servidor do banco de dados
        /// </summary>
        /// <param name="id">Id do servidor a ser removido.</param>
        public void Remove(Int64 id)
        {
            using (var transacao = _context.Database.BeginTransaction())
            {
                try
                {
                    Servidor s = FindById(id);
                    _context.Servidores.Remove(s);
                    _context.SaveChanges();
                    transacao.Commit();
                }
                catch (Exception e)
                {
                    transacao.Rollback();
                    throw  new ServidorException(e.Message);
                }
            }
        }
        /// <summary>
        /// Valida dados de login e senha de um servidor
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="senha">Senha</param>
        /// <returns>Servidor localizado ou null</returns>
        public Servidor Logar(string login, string senha)
        {
            using (CMContext context = new CMContext())
            {
                var user = context.Servidores.Single(s => s.Login.Equals(login) && s.Senha.Equals(senha));
                return user;
            }
        }

        public List<long[]> FindSediado(long servidorId)
        {
            using (CMContext context = new CMContext())
            {
                List<long[]> result = new List<long[]>();
                context.Secretarias.ForEachAsync(sec =>
                {
                    var servidores = sec.Servidores.Select(s => s.IdServidor == servidorId).ToList();
                    if (servidores.Count > 0)
                    {
                        result.Add(new []{sec.Id, sec.IdPrefeitura});
                    }
                });
                return result;
            }
        }
    }
}
