﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Context;
using CMCore.Interfaces;
using CMCore.Model;
using CMRestFull.Class;
using MySql.Data.MySqlClient;

namespace CMCore.Repository
{
    /// <summary>
    /// Classe encarregada de todo processo de persistencia do Protocolo
    /// </summary>
    public class ProtocoloRepository : IProtocoloRepository
    {
        private CMContext _context;

        public ProtocoloRepository(CMContext context)
        {
            this._context = context;
        }

        

        /// <summary>
        /// Persiste os dados de um protocolo no banco de dados - Referenciando ao RF2
        /// </summary>
        /// <param name="protocolo">Protocolo cujos dados serão persistidos</param>
        /// <returns>Protocolo após dados persistidos</returns>
        public Protocolo Save(Protocolo protocolo)
        {
            using (var transacao = _context.Database.BeginTransaction())
            {
                try
                {
                    if (protocolo.Id == 0)
                    {
                        long idPrefeitura = _context.Problemas.Find(protocolo.IdProblema).Secretaria.IdPrefeitura;
                        protocolo.Identification= CriaChave(idPrefeitura);//Define o número do protocolo
                        _context.Protocolos.Add(protocolo);
                      
                    }
                    else
                    {
                        _context.Entry(protocolo).State = EntityState.Modified;
                    }

                    _context.SaveChanges();
                    transacao.Commit();
                }
                catch (Exception e)
                {
                    transacao.Rollback();
                    throw new ProtocoloException(e.InnerException.Message);
                }

                return protocolo;
            }
        }
        /// <summary>
        /// Busca por um protocolo a partir do ID do mesmo
        /// </summary>
        /// <param name="id">Id do protocolo</param>
        /// <returns>Protocolo ou null</returns>
        public Protocolo FindById(Int64 id)
        {
            return _context.Protocolos.Find(id);
        }
        /// <summary>
        /// Busca por um protocolo apartir do e-mail e identificação repassado referenciando ao RF1
        /// </summary>
        /// <param name="email">E-mail cadastrado junto ao protocolo</param>
        /// <param name="identification">Identificação do protocolo</param>
        /// <returns>Protocolo localizado ou null</returns>
        public Protocolo FindByIndentificatioEmail(String email, string identification)
        {
            String SQL = String.Format("Select * from protocolo where email='{0}' and identification = '{1}'", email, identification);
            var result = _context.Protocolos.SqlQuery(SQL);
            return (result != null && result.Count() >0)?result.Single():null;
      
        }
        /// <summary>
        /// Busca por um protocolo apartir do telefone e indentificação do portocolo repassado, refernciado ao RF1
        /// </summary>
        /// <param name="phone">Número do telefone celular que abriu o protocolo</param>
        /// <param name="identification">Identificação do protcolo</param>
        /// <returns>Protocolo ou null quando não identificado</returns>
        public Protocolo FindByIndentificatioPhone(String phone, string identification)
        {
            
            var result = _context.Protocolos.Where(p1 => p1.Identification == identification && p1.Telefone == phone);
            return result.Any()? result.Single(): null;

        }
        /// <summary>
        /// Busca por todos os procedimentos de um determinado protocolo repassado por parametro
        /// </summary>
        /// <param name="phone">Telefone celular vinculado ao protocolo</param>
        /// <param name="identification">Protocolo</param>
        /// <returns>Lista de procedimentos localizados</returns>
        public List<Procedimento> FindProcedimentosbyProtocol(String phone, String identification)
        {
            if (!ValiadaProtocolo(phone, identification))
            {
                throw  new Exception("Telefone ou protocolo informado não foram localizados");
            }
            String SQL = String.Format("select procedimento.* from protocolo left join procedimento on protocolo.id = procedimento.IdProtocolo where protocolo.Identification= '{0}' and protocolo.Telefone ='{1}'", identification, phone);
            var result = _context.Procedimentos.SqlQuery(SQL).ToList();
            return (result.Count > 0) ? result : new List<Procedimento>();
        }
        /// <summary>
        /// Verifica se os dados de identificação e telefone são válidos
        /// </summary>
        /// <param name="phone">Telefone a ser pesquisado</param>
        /// <param name="identification">identificação a ser pesquisado</param>
        /// <returns>Booleando de confirmação</returns>
        private bool ValiadaProtocolo(String phone, String identification)
        {
            int q = _context.Protocolos.Where(p => p.Identification == identification && p.Telefone == phone).ToList().Count();
            return (q != 0);
        }
        /// <summary>
        /// Busca por todos os protocolos cadastrados siguindo o RF9.1
        /// </summary>
        /// <returns>Lista de todos os protocolos</returns>
        public List<Protocolo> FindAll()
        {
            return _context.Protocolos.OrderBy
                (p => p.Procedimentos.Where(c => c.Tipo == TipoProcedimento.REABERTO).Count()).ToList()
                .OrderByDescending(p => p.Datahora).ToList();
        }
        /// <summary>
        /// Lista todos os protocolos de uma secretaria seguindo o RF9.1
        /// </summary>
        /// <param name="idSec">Id da secretaria aplicada como filtro</param>
        /// <returns>Lista de todos os protocolos de um secretaria</returns>
        public List<Protocolo> FindAllbySec(Int64 idSec)
        {
            return FindAll().Where(p => p.Problema.IdSecretaria == idSec).ToList();
        }
        /// <summary>
        /// Busca por todos os protocolos abertos com o status repassado
        /// </summary>
        /// <param name="status">Status a ser filtrado</param>
        /// <returns>Lista de todos os protocolos filtrados pelo status repassados</returns>
        public List<Protocolo> FindAllByStatus(Status status)
        {
            return FindAll().Where(p => p.Status == status).ToList();
        }
        /// <summary>
        /// Busca por protocolos em abertos de todas as secretarias cujo o id do servidor tiver vinculo
        /// </summary>
        /// <param name="idServidor">Id do servidor</param>
        /// <returns>Lista de protocolos</returns>
        public List<Protocolo> FindProtocoloByIdServidor(long idServidor)
        {
                ServidorRepository sr = new ServidorRepository(_context);
                List<Int64[]> sediado = sr.FindSediado(idServidor);
                List<Int64> idsSec = new List<long>();
                foreach (var dados in sediado)
                {
                   idsSec.Add(dados[0]);
                }
                return _context.Protocolos.Where(p1 => idsSec.Contains(p1.Problema.IdSecretaria)).ToList();
        }

        /// <summary>
        /// Busca por todos os protocolos de uma determinada secretaria filtrada por status
        /// </summary>
        /// <param name="status">Status a ser filtrado</param>
        /// <param name="idSec">Id da secretaria responsável pelo protocolo</param>
        /// <returns>Lista de protocolos</returns>
        public List<Protocolo> FindAllByStatusBySec(Status status, Int64 idSec)
        {
            return FindAllByStatus(status).Where(p => p.Problema.IdSecretaria == idSec).ToList();
        }
        /// <summary>
        /// Busca por todos os protocolos que estão fora do prazo
        /// </summary>
        /// <returns>Lista de todos os protocolos fora do prazo.</returns>
        public List<Protocolo> FindAllForaDoPrazo()
        {
            return FindAll().Where(p => p.PrevConclusao > DateTime.Now).ToList();
        }
        /// <summary>
        /// Busca por todos os protocolos fora do prazo de uma determindada secretaria
        /// </summary>
        /// <param name="idSec">Id da Secretaria</param>
        /// <returns>Lista de protocolos vencidos de uma secretaria</returns>
        public List<Protocolo> FindAllForaDoPrazoBySec(Int64 idSec)
        {
            return FindAllForaDoPrazo().Where(p => p.Problema.IdSecretaria == idSec).ToList();
        }
        /// <summary>
        /// Cria a sequencia numérica da identificação do protocolo.
        /// </summary>
        /// <param name="idPrefeitura">id da Prefeitura a ser usado</param>
        /// <returns>Chave composta por YYYYMMDDXXXX</returns>
        private String CriaChave(Int64 idPrefeitura)
        {
            String SQL = String.Format("select count(cast(protocolo.Datahora as Date)) as quantidade " +
            "from protocolo left join (select secretaria.IdPrefeitura, problema.Id as IdProblema from problema left join secretaria on secretaria.Id = problema.IdSecretaria " +
            ") as sec on protocolo.IdProblema = sec.IdProblema "+
            "where sec.IdPrefeitura = {0} and cast(protocolo.Datahora as Date) = cast(now() as Date) "+
            "group by cast(protocolo.Datahora as Date)", idPrefeitura);
            
            var q = _context.Database.SqlQuery<long?>(SQL);
            String numero = "00000" + (((q.Count()==0)?0:q.Single())+1);
            numero = numero.Substring((numero.Length-5), 5);
            String result = String.Format("{0}{1}{2}{3}", DateTime.Now.Year,(DateTime.Now.Month<10?"0":"")+ DateTime.Now.Month,(DateTime.Now.Day<10?"0":"") + DateTime.Now.Day, numero);
            return result;
        }

        /// <summary>
        /// Busca por todos os protocolos não lidos e não notificados
        /// </summary>
        /// <returns>Lista os protocolos não notificados e não lidos</returns>
        public List<Protocolo> FindAllNotNotification()
        {
            var result = _context.Protocolos.Where(p => !p.Lido && !p.Notificado).ToList();
            return result;
        }

        /// <summary>
        /// Muda os status dos protocolos repassador como notificados
        /// </summary>
        /// <param name="identifications">Lista de indentifcações repassado</param>
        public void ConfirmaEnvioNotificacao(String[] identifications)
        {
            foreach (string identification in identifications)
            {
                Protocolo protocolo = FindAll().Find(p => p.Identification.Equals(identification));
                if (protocolo != null)
                {
                    protocolo.Notificado = true;
                    _context.Entry(protocolo).State = EntityState.Modified;
                    
                }
            }
            _context.SaveChanges();
        }

        /// <summary>
        /// Busca por um protocolo apartir do identification repassado como parametro
        /// </summary>
        /// <param name="identification">Identification do protocolo</param>
        /// <returns>Protocolo ou null</returns>
        public Protocolo FindyByIdentification(string identification)
        {
            var resp = _context.Protocolos.Where(p => p.Identification == identification);
            return resp.Count() > 0 ? resp.Single() : null;
        }
    }
}
