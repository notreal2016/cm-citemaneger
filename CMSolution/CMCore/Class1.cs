﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Context;
using CMCore.Repository;
using CMCore.Service;
using CMCore.Model;

namespace CMCore
{
    public class Class1
    {
       

        public static void Main(String[] args)
        {
         CriaProtocolo();   
        }
        public static void CriaProtocolo()
        {
            CMContext context = new CMContext();
            ProtocoloService ps = new ProtocoloService(new ProtocoloRepository(context), new SecretariaRepository(context));
            Protocolo protocolo = new Protocolo()
            {
                Problema    = ps.FindProblemaById(1),
                Email = "laertonmarques@uol.com.br",
                Endereco = new Endereco() {Bairro = "Centro", Cep = "58800-280", Cidade = "Sousa", Latitude = Decimal.Parse("10.57855"), Logradouro = "R. de teste", Longitude = (decimal)63.23f, Numero = 10, Uf = "PB"},
                Observacao = "Este modulo foi implantado por um teste",
                PontoReferencia = "Este modulo foi implantado por um teste",
                Telefone = "83991076105",
                
            };
            ps.Save(protocolo);
            Console.WriteLine(protocolo.Identification);
            Console.ReadLine();
        }       
    }
}
