﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Context;

namespace CMCore.Factory
{
    /// <summary>
    /// Classe encarregada produzir um CMContext
    /// </summary>
    public class FactoryContext
    {
        private static CMContext _context;

        /// <summary>
        /// Fabrica um CMContext
        /// </summary>
        /// <returns>CmContext</returns>
        public static CMContext FactoryCmContext()
        {
            if (_context == null)
            {
                _context = new CMContext();
            }

            return _context;
        }

        public static void Disponse()
        {
            if (_context != null) _context.Dispose();
            _context = null;
        }

        
    }
}
