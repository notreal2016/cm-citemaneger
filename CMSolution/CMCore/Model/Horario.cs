﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace CMCore.Model
{
    /// <summary>
    /// Classe contendo dados de horarios de trabalho
    /// </summary>
    [Table("Horario"),Serializable, JsonObject]
    public class Horario
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private String _descricao;
        private TimeSpan _abertura;
        private TimeSpan _fechamento;
        private TimeSpan _inicioIntervalo;
        private TimeSpan _fimDoIntervalo;
        private List<DayOfWeek> _diasDaSemana = new List<DayOfWeek>();

        public void addDiaDaSemana(DayOfWeek dia)
        {
            if (!_diasDaSemana.Contains(dia))
            {
                _diasDaSemana.Add(dia);
            }
        }

        public void remDiaDaSemana(DayOfWeek dia)
        {
            _diasDaSemana.Remove(dia);

        }

        public void remAllDiasDaSemana()
        {
            _diasDaSemana = new List<DayOfWeek>();
        }

        

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Descricao
        {
            get { return _descricao; }
            set { _descricao = value; }
        }
        [Column(TypeName = "Time")]
        public TimeSpan Abertura
        {
            get { return _abertura; }
            set { _abertura = value; }
        }
        [Column(TypeName = "Time")]
        public TimeSpan Fechamento
        {
            get { return _fechamento; }
            set { _fechamento = value; }
        }
        [Column(TypeName = "Time")]
        public TimeSpan InicioIntervalo
        {
            get { return _inicioIntervalo; }
            set { _inicioIntervalo = value; }
        }
        [Column(TypeName = "Time")]
        public TimeSpan FimDoIntervalo
        {
            get { return _fimDoIntervalo; }
            set { _fimDoIntervalo = value; }
        }
        
        public List<DayOfWeek> DiasDaSemana
        {
            get
            {
                
                return _diasDaSemana;
            }
            set { _diasDaSemana = value; }
        }
    }
}