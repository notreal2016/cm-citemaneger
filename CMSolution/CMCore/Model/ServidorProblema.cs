﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMCore.Model
{
    [Table("ServidorProblema")]
    public class ServidorProblema
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        
        private Int64 _idServidor;
        private Int64 _idProblema;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public long IdServidor
        {
            get { return _idServidor; }
            set { _idServidor = value; }
        }

        public long IdProblema
        {
            get { return _idProblema; }
            set { _idProblema = value; }
        }
        [ForeignKey("IdServidor")]
        
        public virtual Servidor Servidor { get; set; }
        [ForeignKey("IdProblema")]
        
        public virtual Problema Problema { get; set; }

    }
}
