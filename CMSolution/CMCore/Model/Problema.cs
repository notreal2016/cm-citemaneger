﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CMCore.Model
{
    /// <summary>
    /// Classe encarregada de armazenas os dados de um problema
    /// </summary>
    [Table("Problema"), Serializable, JsonObject]
    public class Problema
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private Int64 _idSecretaria;
        private String _descricao;
        private int _tempoConclusao;
        private bool _corrido = false;
        private bool _ativo = true;
        private List<ServidorProblema> _servidores = new List<ServidorProblema>();
        private TipoProblema _tipo;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public TipoProblema Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        public void AddServidor(Servidor servidor)
        {
            _servidores.Add(new ServidorProblema() {Servidor = servidor, Problema = this});
        }

        public void remServidor(Servidor servidor)
        {
            ServidorProblema sp = _servidores.Where(s => s.Servidor == servidor).Single();
            if (sp != null)
            {
                _servidores.Remove(sp);
            }
            
        }

        public virtual List<ServidorProblema> Servidores
        {
            get { return _servidores; }
            set { _servidores = value; }
        }
        
        public long IdSecretaria
        {
            get { return _idSecretaria; }
            set { _idSecretaria = value; }
        }

        public string Descricao
        {
            get { return _descricao; }
            set { _descricao = value; }
        }

        public int TempoConclusao
        {
            get { return _tempoConclusao; }
            set { _tempoConclusao = value; }
        }

        public bool Corrido
        {
            get { return _corrido; }
            set { _corrido = value; }
        }

        public bool Ativo
        {
            get { return _ativo; }
            set { _ativo = value; }
        }

        [ForeignKey("IdSecretaria")]
        public virtual  Secretaria Secretaria { get; set; }
        public override string ToString()
        {
            return Descricao.ToUpper();
        }
    }
}
