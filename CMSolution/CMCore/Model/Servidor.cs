﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CMCore.Model
{
    /// <summary>
    /// Classe contendo dados de um Servido
    /// </summary>
    [Table("Servidor"), Serializable, JsonObject]
    public class Servidor
    {
        private Int64 _id;
        private String _login;
        private String _senha;
        private String _nome;
        private String _email;
        private String _celular;
        private bool _bloqueado = false;
        private TipoServidor _tipo;
        private String _matricula;

        public Servidor()
        {
        }

        [Index(IsUnique = true), MaxLength(10)]
        public string Matricula
        {
            get { return _matricula; }
            set { _matricula = value; }
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        public string Senha
        {
            get { return _senha; }
            set { _senha = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }
        [Index(IsUnique = true), MaxLength(255)]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        [Index(IsUnique = true), MaxLength(12)]
        public string Celular
        {
            get { return _celular; }
            set { _celular = value; }
        }

        public bool Bloqueado
        {
            get { return _bloqueado; }
            set { _bloqueado = value; }
        }

        public TipoServidor Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }
    }
}
