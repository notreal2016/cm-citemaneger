﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CMCore.Model
{
    /// <summary>
    /// Classe com os dados de uma prefeitura
    /// </summary>
    [Table("Prefeitura"), Serializable, JsonObject]
    public class Prefeitura
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private String _nome;
        private Endereco _endereco;
        private Int64 _idPrefeito;
        private List<Secretaria> _secretarias = new List<Secretaria>();

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        public Endereco Endereco
        {
            get { return _endereco; }
            set { _endereco = value; }
        }

        public long IdPrefeito
        {
            get { return _idPrefeito; }
            set { _idPrefeito = value; }
        }

        [ForeignKey("IdPrefeito")]
        public virtual Servidor Prefeito { get; set; }

        public void AddSecretaria(Secretaria secretaria)
        {
            _secretarias.Add(secretaria);
        }

        public void RemoveSecretaria(Secretaria secretaria)
        {
            Secretaria s = _secretarias.Single(s1 => s1.Id == secretaria.Id);
            if (s != null)
            {
                _secretarias.Remove(s);
            }
        }
        public virtual List<Secretaria> Secretarias
        {
            get { return _secretarias; }
            set { _secretarias = value; }
        }
    }
}
