﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CMCore.Model
{
    [ComplexType]
    public class Endereco
    {
        private String _logradouro;
        private Int32 _numero;
        private String _bairro;
        private String _cep;
        private String _cidade;
        private String _uf;
        private decimal _latitude;
        private decimal _longitude;

        

        public string Logradouro
        {
            get { return _logradouro; }
            set { _logradouro = value; }
        }

        public int Numero
        {
            get { return _numero; }
            set { _numero = value; }
        }

        public string Bairro
        {
            get { return _bairro; }
            set { _bairro = value; }
        }

        public string Cep
        {
            get { return _cep; }
            set { _cep = value; }
        }

        public string Cidade
        {
            get { return _cidade; }
            set { _cidade = value; }
        }

        public string Uf
        {
            get { return _uf; }
            set { _uf = value; }
        }

        [DataType("DECIMAL(10,8)")]
        public decimal Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        [DataType("DECIMAL(11,8)")]
        public decimal Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
    }
}