﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMCore.Model
{
    [Table("ServidorSecretaria")]
    public class ServidorSecretaria
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;

        private Int64 _idServidor;
        private Int64? _idSecretaria;
        private Int64 _idPrefeitura;

        [ForeignKey("IdPrefeitura")]
        public virtual Prefeitura Prefeitura { get; set; }
        [ForeignKey("IdSecretaria")]
        public virtual Secretaria Secretaria { get; set; }
        [ForeignKey("IdServidor")]
        public virtual Servidor Servidor { get; set; }
        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public long IdServidor
        {
            get { return _idServidor; }
            set { _idServidor = value; }
        }

        public long? IdSecretaria
        {
            get { return _idSecretaria; }
            set { _idSecretaria = value; }
        }

        public long IdPrefeitura
        {
            get { return _idPrefeitura; }
            set { _idPrefeitura = value; }
        }
    }
}
