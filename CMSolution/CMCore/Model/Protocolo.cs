﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace CMCore.Model
{
    /// <summary>
    /// Classe encarregada de cuidar dos dados de um protocolo aberto
    /// </summary>
    [Table("Protocolo"), Serializable, JsonObject]
    public class Protocolo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private String _identification;
        private Int64 _idProblema;
        
        private Endereco _endereco;
        private String _telefone;
        private String _pontoReferencia;
        private String _email;
        private DateTime _datahora = DateTime.Now;
        private byte[] _arquivoImg;
        private DateTime _prevConclusao;
        private Status _status = Status.ABERTO;
        private List<Procedimento> _procedimentos = new List<Procedimento>();
        private TipoAvaliacao _avaliacao = TipoAvaliacao.NAOAVALIADO;
        private bool _notificado = false;
        private bool _lido = false;
        private String _observacao = String.Empty;

        public string Observacao
        {
            get { return _observacao; }
            set { _observacao = value; }
        }

        public Protocolo()
        {
            _endereco = new Endereco();
        }

        public TipoAvaliacao Avaliacao
        {
            get { return _avaliacao; }
            set { _avaliacao = value; }
        }

        public bool Notificado
        {
            get { return _notificado; }
            set { _notificado = value; }
        }

        public bool Lido
        {
            get {return _lido;}
            set { _lido = value; }
        }

        [Index(IsUnique = true), MaxLength(13)]
        public string Identification
        {
            get { return _identification; }
            set { _identification = value; }
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }
        [Column("IdProblema")]
        [Required]
        public long IdProblema
        {
            get { return (Problema != null)?Problema.Id:_idProblema; }
            set { _idProblema = value; }
        }

        [ForeignKey("IdProblema")]
        public virtual Problema Problema { get; set; }

        
        public Endereco Endereco
        {
            get { return _endereco; }
            set { _endereco = value; }
        }

        public string Telefone
        {
            get { return _telefone; }
            set { _telefone = value; }
        }

        public string PontoReferencia
        {
            get { return _pontoReferencia; }
            set { _pontoReferencia = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public DateTime Datahora
        {
            get { return _datahora; }
            set { _datahora = value; }
        }

        public byte[] ArquivoImg
        {
            get { return _arquivoImg; }
            set { _arquivoImg = value; }
        }

        public DateTime PrevConclusao
        {
            get { return _prevConclusao; }
            set { _prevConclusao = value; }
        }

        public Status Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public virtual List<Procedimento> Procedimentos => _procedimentos;

        public void addProcedimento(Procedimento procedimento)
        {
            _procedimentos.Add(procedimento);
        }

        public void remProcedimento(Procedimento procedimento)
        {
            _procedimentos.Remove(procedimento);
        }

        public void remProcedimento(Int64 _idProcedimento)
        {
            Procedimento p = _procedimentos.Where(p1 => p1.Id == _idProcedimento).Single();

            if (p != null)
            {
                _procedimentos.Remove(p);
            }
            
        }
        
    }

}
