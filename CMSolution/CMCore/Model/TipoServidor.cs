﻿namespace CMCore.Model
{
    /// <summary>
    /// Tipos de servidores registrados.
    /// </summary>
    public enum TipoServidor
    {
        FUNCIONARIO =0, SECRETARIO =1 , PREFEITO =2, ADMINISTRADOR =3
    }
}