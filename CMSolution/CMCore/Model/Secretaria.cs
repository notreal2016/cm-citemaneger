﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.Entity;
using Newtonsoft.Json;

namespace CMCore.Model
{
    /// <summary>
    /// Classe contendo os dados de uma secretaria.
    /// </summary>
    [Table("Secretaria"), Serializable, JsonObject]
    public class Secretaria
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private String _nome;
        private CMCore.Model.Endereco _endereco ;
        private String _telefone;
        private String _email;
        private Int64 _idSecretario;
        private Int64 _idHorario;
        private Int64 _idPrefeitura;
        private List<Problema> _problemas = new List<Problema>();
        private List<ServidorSecretaria> _servidores = new List<ServidorSecretaria>();
        public Secretaria()
        {
            _endereco = new Endereco();
        }

        public virtual List<Problema> Problemas
        {
            get { return _problemas; }
            set { _problemas = value; }
        }

        public void AddProblema(Problema problema)
        {
            _problemas.Add(problema);
        }

        public void RemProblema(Problema problema)
        {
            Problema pr = _problemas.Single(p => p.Id == problema.Id);
            _problemas.Remove(pr);
        }
        /// <summary>
        /// Vincula um servidor a esta secretaria
        /// </summary>
        /// <param name="servidor"></param>
        public void VinculaServidor(Servidor servidor)
        {
            _servidores.Add(new ServidorSecretaria() {Servidor = servidor, IdSecretaria = this.Id, IdPrefeitura = this.Prefeitura.Id});
        }
        /// <summary>
        /// Desvicula o servidor desta secretaria
        /// </summary>
        /// <param name="servidor"></param>
        public void DesviculaServidor(Servidor servidor)
        {
            ServidorSecretaria ss = _servidores.Single(s => s.IdServidor == servidor.Id && (s.IdSecretaria == Id));
            if (ss != null)
            {
                _servidores.Remove(ss);
            }
        }

        public virtual List<ServidorSecretaria> Servidores
        {
            get { return _servidores; }
            set { _servidores = value; }
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }
        
        public CMCore.Model.Endereco Endereco
        {
            get { return _endereco; }
            set { _endereco = value; }
        }

        public string Telefone
        {
            get { return _telefone; }
            set { _telefone = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        
        public long IdSecretario
        {
            get { return _idSecretario; }
            set { _idSecretario = value; }
        }

        [ForeignKey("IdSecretario")]
        public virtual Servidor Secretario
        {
            get;
            set;
        }
        [ForeignKey("IdHorario")]
        public virtual Horario Horario { get; set; }

        public long IdHorario
        {
            get { return _idHorario; }
            set { _idHorario = value; }
        }

        public long IdPrefeitura
        {
            get { return _idPrefeitura; }
            set { _idPrefeitura = value; }
        }

        [ForeignKey("IdPrefeitura")]
        public virtual Prefeitura Prefeitura { get; set; }
    }
}
