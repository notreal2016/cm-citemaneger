﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace CMCore.Model
{
    /// <summary>
    /// Classe encarregada de cuidar dos dados de um procedimeto que ficará vinculado a um protocolo
    /// </summary>
    [Table("Procedimento"), Serializable, JsonObject]
    public class Procedimento
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;

        private Int64? _idServidor;
        private DateTime _datahora = DateTime.Now;
        private String _descricao;
        private Int64 _idProtocolo;
        private TipoProcedimento _tipo;
        private int _tempoExtra;
        

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }
        
        public long? IdServidor
        {
            get { return _idServidor; }
            set { _idServidor = value; }
        }
        [ForeignKey("IdServidor")]
        public virtual Servidor Servidor { get; set; }
        public DateTime Datahora
        {
            get { return _datahora; }
            set { _datahora = value; }
        }

        public string Descricao
        {
            get { return _descricao; }
            set { _descricao = value; }
        }
        

        public long IdProtocolo
        {
            get { return _idProtocolo; }
            set { _idProtocolo = value; }
        }
        [ForeignKey("IdProtocolo")]
        public virtual Protocolo Protocolo { get; set; }

        public TipoProcedimento Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        public int TempoExtra
        {
            get { return _tempoExtra; }
            set { _tempoExtra = value; }
        }
    }
}