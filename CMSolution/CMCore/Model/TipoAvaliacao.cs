﻿namespace CMCore.Model
{
    /// <summary>
    /// Classe enum de avaliação de satisfação 
    /// </summary>
    public enum TipoAvaliacao
    {
        INSATISFEITO=0, NEUTRO =1 , SATISFEITO=2, NAOAVALIADO =3
    }
}