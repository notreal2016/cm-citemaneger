﻿using System;
using System.Collections.Generic;
using CMCore.Context;
using CMCore.Model;

namespace CMCore.Repository
{
    public interface ISecretariaRepository
    {
        /// <summary>
        /// Persiste os dados de uma secretaria no banco de dados 
        /// </summary>
        /// <param name="secretaria">Dados da secretaria que será persistido no banco de dados</param>
        /// <returns>Secretaria após dados persistidos</returns>
        Secretaria Save(Secretaria secretaria);

        /// <summary>
        /// Busca por uma secretaria apartir do id Repassado
        /// </summary>
        /// <param name="id">Id da secretaria</param>
        /// <returns>Objeto Secretaria ou null caso não seja encontrado</returns>
        Secretaria FindById(Int64 id);

        /// <summary>
        /// Busca por todas as secretarias de uma prefeitura
        /// </summary>
        /// <param name="idPrefeitura">Id da prefeitura em questão</param>
        /// <returns>Lista de secretarias cadastradas</returns>
        List<Secretaria> AllSecretariasByPrefeitura(Int64 idPrefeitura);

        /// <summary>
        /// Remove uma secretária apartir do Id repassado
        /// </summary>
        /// <param name="id">Id da secretaria removida.</param>
        void Remove(Int64 id);

        /// <summary>
        /// Lista todos os problemas cadastros pelo tipo repassado como paramentro
        /// </summary>
        /// <param name="tipo">Tipo de problema a ser filtrado</param>
        /// <returns>Lista de problemas cadastrados</returns>
        List<Problema> FindByTipoProblema(TipoProblema tipo, CMContext context);
        /// <summary>
        /// Busca por um problema a partir do Id repassado
        /// </summary>
        /// <param name="id">Id do problema a ser localizado</param>
        /// <returns>Problema localizado ou null</returns>
        Problema FindProblemaById(long id);
    }
}