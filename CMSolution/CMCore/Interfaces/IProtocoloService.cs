﻿using System;
using System.Collections.Generic;
using CMCore.Componentes;
using CMCore.Context;
using CMCore.Model;
using CMCore.EntidadeSingle;

namespace CMCore.Service
{
    public interface IProtocoloService
    {
        /// <summary>
        /// Salva os dados de um protocolo
        /// </summary>
        /// <param name="protocolo">Protocolo cujos dados serão salvos</param>
        /// <returns>Protocolo após ser salvo</returns>
        Protocolo Save(Protocolo protocolo);

        /// <summary>
        /// Busca por todos os procedimentos de um determinado protocolo repassado por parametro
        /// </summary>
        /// <param name="phone">Telefone celular vinculado ao protocolo</param>
        /// <param name="identification">Protocolo</param>
        /// <returns>Lista de procedimentos localizados</returns>
        List<Procedimento> FindProcedimentosbyProtocol(String phone, String identification);

        /// <summary>
        /// Busca um protocolo pelo Idprotocolo e email do cidadão
        /// </summary>
        /// <param name="IdProtocolo">Id do protocolo enviado ao cidadão</param>
        /// <param name="email">Email do cidadão</param>
        /// <returns>Protocolo referenciado ou null</returns>
        Protocolo FindByIDAndEmail(String IdProtocolo, String email);

        /// <summary>
        /// Busca um protocolo pelo IdProtocolo e Telefone celilar do cidadão
        /// </summary>
        /// <param name="IdProtocolo">Id Protocolo</param>
        /// <param name="phone">Telfone do cidadãos</param>
        /// <returns>Protocolo referenciado ou null</returns>
        Protocolo FindByIdAndPhone(String IdProtocolo, String phone);

        /// <summary>
        /// Lista todos os Status em ordem cronologica dos processos vinculados a um protocolo
        /// Atendendo ao RF1
        /// </summary>
        /// <param name="idProtocolo">id protocolos</param>
        /// <returns>Lista de status</returns>
        List<StatusProtocolo> Status(Int64 idProtocolo);
        /// <summary>
        /// Busca por todos os problemas relacionado ao tipo repassado como paramentro.
        /// </summary>
        /// <param name="tipo">Tipo de problema a ser filtrado</param>
        /// <returns>Lista de problemas localizados pelo tipo repassado</returns>
        List<Problema> ListProblemasByTipo(TipoProblema tipo, CMContext context = null);
        /// <summary>
        /// Atualiza protocolos já notificados e busca pelos não notificados 
        /// </summary>
        /// <param name="notifications">Notificações que foram já realizadas</param>
        /// <returns>Notificações que ainda não foram realizadas</returns>
        List<ProtoTele> UpNotAndFindNotNotification(String[] notifications = null);
        
        /// <summary>
        /// Busca por um problema pelo Id Repassado
        /// </summary>
        /// <param name="id">Id do problema a ser localizado</param>
        /// <returns>Problema localizado ou Null</returns>
        Problema FindProblemaById(Int64 id);
        /// <summary>
        /// Busca por um protocolo apartir do Identification repassado
        /// </summary>
        /// <param name="identification">Identification do protocolo a ser buscado</param>
        /// <returns>Protocolo ou null</returns>
        Protocolo FindByIdentification(String identification);
    }
}