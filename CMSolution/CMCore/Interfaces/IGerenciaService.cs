﻿using System;
using System.Collections.Generic;
using CMCore.EntidadeSingle;
using CMCore.Model;
using Newtonsoft.Json.Linq;

namespace CMCore.Service
{
    public interface IGerenciaService
    {
        /// <summary>
        /// Loga o servidor ao sistema
        /// </summary>
        /// <param name="login">Login </param>
        /// <param name="senha">Senha</param>
        /// <returns>Servidor ou null</returns>
        Servidor Logar(string login, string senha);
        List<SecResume> FindResumeSecretariaByServidor(Int64 id);
        /// <summary>
        /// Método encarregado de adicionar um novo servidor ao sistema
        /// </summary>
        /// <param name="servidor">Servidor que será adicionado</param>
        Servidor AddServidor(Servidor servidor);
        /// <summary>
        /// Salva os dados de um prefeitura no banco de dados 
        /// </summary>
        /// <param name="prefeitura">Prefeitura cujos dados serão salvos</param>
        /// <returns>Prefeitura</returns>
        Prefeitura SavePrefeitura(Prefeitura prefeitura);
        /// <summary>
        /// Salva os dados de uma secretaria.
        /// </summary>
        /// <param name="secretaria">Secretaria cujos dados serão salvos</param>
        /// <returns>Secretaria</returns>
        Secretaria SaveSecretaria(Secretaria secretaria);
        /// <summary>
        /// Busca por um servidor pelo id repassado 
        /// </summary>
        /// <param name="id">Id do servidor</param>
        /// <returns>Servidor ou null</returns>
        Servidor FidServidorById(long id);
        /// <summary>
        /// Vincula um servidor a uma secretária pelos ids repassados respectivamente nos argumentos
        /// </summary>
        /// <param name="idServidor">id do servidor repassado</param>
        /// <param name="idSecretaria">id da secretaria repassada</param>
        void VinculaServidor(long idServidor, long idSecretaria);
        /// <summary>
        /// Lista de todas as secretarias de uma determinada prefeitura
        /// </summary>
        /// <returns>Lista das secretarias</returns>
        List<JObject> GetAllSecretarias(Int64 idPrefeitura);
        
    }
}