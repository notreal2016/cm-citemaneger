﻿using System;
using System.Collections.Generic;
using CMCore.Model;

namespace CMCore.Repository
{
    public interface IServidorRepository
    {
        /// <summary>
        /// Persiste os dados de um servidor no banco de dados
        /// </summary>
        /// <param name="servidor">Servidor cujo os dados serão persistidos</param>
        /// <returns>Servidor ápos a persistencia dos dados</returns>
        Servidor Save(Servidor servidor);

        /// <summary>
        /// Busca por um servidor apartir do id repassado
        /// </summary>
        /// <param name="id">Id do servidor</param>
        /// <returns>Servidor localizado ou null caso não seja localizado</returns>
        Servidor FindById(Int64 id);

        /// <summary>
        /// Busca todos os servidores de uma secretaria em ordem alfabetica
        /// </summary>
        /// <param name="idSecretaria">Id da secretaria a ser pesquisada</param>
        /// <returns>Lista de servidores</returns>
        List<Servidor> FindAllServidoresBySecretaria(Int64 idSecretaria);

        /// <summary>
        /// Busca todos os servidores vinculados a uma prefeitura
        /// </summary>
        /// <param name="idPrefeitura">Id da prefeitura repassada como paramentro</param>
        /// <returns>Lista de todos os servidores da prefeitura informada pelo id.</returns>
        List<Servidor> FindAllServidoresByPrefeitura(Int64 idPrefeitura);

        /// <summary>
        /// Remove um servidor do banco de dados
        /// </summary>
        /// <param name="id">Id do servidor a ser removido.</param>
        void Remove(Int64 id);
        /// <summary>
        /// Valida o Login e senha do servidor no sistema
        /// </summary>
        /// <param name="login">login do servidor</param>
        /// <param name="senha">Senha do servidor</param>
        /// <returns>Servidor</returns>
        Servidor Logar(string login, string senha);
        /// <summary>
        /// Extrai os dados de locação de um servidor pelo id repassado
        /// </summary>
        /// <param name="servidorId">Id do servidor</param>
        /// <returns>Dados de um servidor</returns>
        List<long[]> FindSediado(long servidorId);
    }
}