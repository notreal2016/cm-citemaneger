﻿using System;
using CMCore.Model;

namespace CMCore.Interfaces
{
    public interface IPrefeituraRepository
    {
        /// <summary>
        /// Persiste os dados de um prefeitura no banco de dados
        /// </summary>
        /// <param name="prefeitura">Dados da prefeitura que serão persistidos</param>
        /// <returns>Objeto prefeitura após a persistência</returns>
        Prefeitura Save(Prefeitura prefeitura);

        /// <summary>
        /// Busca uma prefeitura apartir do id repassado
        /// </summary>
        /// <param name="idPrefeitura">Id da prefeitura</param>
        /// <returns>Prefeitura localizada ou Null quando não localizado</returns>
        Prefeitura FindById(Int64 idPrefeitura);

        /// <summary>
        /// Remove os dados de uma prefeitura do banco de dados
        /// </summary>
        /// <param name="id">Id da refeitura</param>
        void Remove(Int64 id);
    }
}