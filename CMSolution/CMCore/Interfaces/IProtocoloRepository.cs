﻿using System;
using System.Collections.Generic;
using CMCore.Model;

namespace CMCore.Interfaces
{
    public interface IProtocoloRepository
    {
        /// <summary>
        /// Persiste os dados de um protocolo no banco de dados - Referenciando ao RF2
        /// </summary>
        /// <param name="protocolo">Protocolo cujos dados serão persistidos</param>
        /// <returns>Protocolo após dados persistidos</returns>
        Protocolo Save(Protocolo protocolo);

        /// <summary>
        /// Busca por todos os procedimentos de um determinado protocolo repassado por parametro
        /// </summary>
        /// <param name="phone">Telefone celular vinculado ao protocolo</param>
        /// <param name="identification">Protocolo</param>
        /// <returns>Lista de procedimentos localizados</returns>
        List<Procedimento> FindProcedimentosbyProtocol(String phone, String identification);

        /// <summary>
        /// Busca por um protocolo a partir do ID do mesmo
        /// </summary>
        /// <param name="id">Id do protocolo</param>
        /// <returns>Protocolo ou null</returns>
        Protocolo FindById(Int64 id);

        /// <summary>
        /// Busca por um protocolo apartir do e-mail e identificação repassado referenciando ao RF1
        /// </summary>
        /// <param name="email">E-mail cadastrado junto ao protocolo</param>
        /// <param name="identification">Identificação do protocolo</param>
        /// <returns>Protocolo localizado ou null</returns>
        Protocolo FindByIndentificatioEmail(String email, string identification);

        /// <summary>
        /// Busca por um protocolo apartir do telefone e indentificação do portocolo repassado, refernciado ao RF1
        /// </summary>
        /// <param name="phone">Número do telefone celular que abriu o protocolo</param>
        /// <param name="identification">Identificação do protcolo</param>
        /// <returns>Protocolo ou null quando não identificado</returns>
        Protocolo FindByIndentificatioPhone(String phone, string identification);

        /// <summary>
        /// Busca por todos os protocolos cadastrados siguindo o RF9.1
        /// </summary>
        /// <returns>Lista de todos os protocolos</returns>
        List<Protocolo> FindAll();

        /// <summary>
        /// Lista todos os protocolos de uma secretaria seguindo o RF9.1
        /// </summary>
        /// <param name="idSec">Id da secretaria aplicada como filtro</param>
        /// <returns>Lista de todos os protocolos de um secretaria</returns>
        List<Protocolo> FindAllbySec(Int64 idSec);

        /// <summary>
        /// Busca por todos os protocolos abertos com o status repassado
        /// </summary>
        /// <param name="status">Status a ser filtrado</param>
        /// <returns>Lista de todos os protocolos filtrados pelo status repassados</returns>
        List<Protocolo> FindAllByStatus(Status status);
        /// <summary>
        /// Busca por todos os protocos em aberto das secretarias que o servidor tem vinculo
        /// </summary>
        /// <param name="idServidor">Id do servidor</param>
        /// <returns>Lista de protocolos</returns>
        List<Protocolo> FindProtocoloByIdServidor(long idServidor);

        /// <summary>
        /// Busca por todos os protocolos de uma determinada secretaria filtrada por status
        /// </summary>
        /// <param name="status">Status a ser filtrado</param>
        /// <param name="idSec">Id da secretaria responsável pelo protocolo</param>
        /// <returns>Lista de protocolos</returns>
        List<Protocolo> FindAllByStatusBySec(Status status, Int64 idSec);

        /// <summary>
        /// Busca por todos os protocolos que estão fora do prazo
        /// </summary>
        /// <returns>Lista de todos os protocolos fora do prazo.</returns>
        List<Protocolo> FindAllForaDoPrazo();

        /// <summary>
        /// Busca por todos os protocolos fora do prazo de uma determindada secretaria
        /// </summary>
        /// <param name="idSec">Id da Secretaria</param>
        /// <returns>Lista de protocolos vencidos de uma secretaria</returns>
        List<Protocolo> FindAllForaDoPrazoBySec(Int64 idSec);
        /// <summary>
        /// Busca por todos os protocolos que não foram notificados
        /// </summary>
        /// <returns>Lista de protocolos</returns>
        List<Protocolo> FindAllNotNotification();
        /// <summary>
        /// Confirma Envio de notificação 
        /// </summary>
        /// <param name="identifications">Array de Identifications a serem confirmados a notificação</param>
        void ConfirmaEnvioNotificacao(String[] identifications);
        /// <summary>
        /// Busca por um protocolo apartir do identifiation repassado
        /// </summary>
        /// <param name="identification">Identification do protocolo a ser pesquisado</param>
        /// <returns>Protocolo ou null</returns>
        Protocolo FindyByIdentification(string identification);
    }
}