﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CMCore.EntidadeSingle;
//using System.Web.Mvc;
using CMCore.Model;
using CMCore.Service;
using CMRestFull.Class;
using Newtonsoft.Json.Linq;

namespace CMRestFull.Controllers
{
    public class GerenciaController : ApiController
    {
        private IGerenciaService _gs;

        public GerenciaController(IGerenciaService gs)
        {
            _gs = gs;
        }

        /// <summary>
        /// Efetua  login do servidor no sistema 
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="senha">Senha</param>
        /// <returns>Servidor ou null</returns>
        [AcceptVerbs("GET")]
        [Route("Logar/{login}/{senha}")]
        public Servidor Logar(string login, string senha)
        {
            if (!ValidaDados.ExistCaracterEspeciais(login) && !senha.Contains("%"))
            {
                return _gs.Logar(login, senha);
            }
            return null;
        }

        /// <summary>
        /// Busca por protocolos destinados a uma secretaria vinculadas a um servidor
        /// </summary>
        /// <param name="id">Id do servidor</param>
        /// <returns>Lista de Protocolos</returns>
        [AcceptVerbs("GET")]
        [Route("FindProtocolos/{id}")]
        public List<SecResume> FindProtocolosByServidor(Int64 id)
        {
            return _gs.FindResumeSecretariaByServidor(id);
        }

        /// <summary>
        /// Salva os dados de um servidor no sistema
        /// </summary>
        /// <param name="servidor">Dados do servidor salvo</param>
        /// <returns>Mensagen com a confirmação e o ID do servidor, caso não mensagem de erro;</returns>
        [AcceptVerbs("POST")]
        [Route("AddServidor")]
        public  HttpResponseMessage SaveServidor(Servidor servidor)
        {
            try
            {
                servidor = _gs.AddServidor(servidor);
                Ok();
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    ReasonPhrase = "100|Servidor incluso com sucesso!|" + servidor.Id 
                };
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = String.Format("500|Servidor não incluso. Erro:{0}-{1}", e.HResult, e.Message) 
                };    
            }
        }

        /// <summary>
        /// Vincula um servidor a uma secretaria pelo id repassado respectivamente
        /// </summary>
        /// <param name="idServidor">Id do servidor</param>
        /// <param name="idSecretaria">Id da secretaria</param>
        /// <returns>Mensagem de resposta</returns>
        [AcceptVerbs("GET")]
        [Route("Gerente/VinculaServidor/{idServidor},{idSecretaria}")]
        public String VinculaServidor(Int64 idServidor, Int64 idSecretaria)
        {
            try
            {
                _gs.VinculaServidor(idServidor, idSecretaria);
                return "100|Servidor Registrado com sucesso";
            }
            catch (Exception e)
            {
                return "500|Erro ao registrar servidor: " + e.Message;
            }
            
        }
        /// <summary>
        /// Lista de todas as secretarias de uma prefeitura
        /// </summary>
        /// <param name="idPrefeitura">Id da prefeitura</param>
        /// <returns>Lista das secretarias.</returns>
        [AcceptVerbs("GET")]
        [Route("Gerente/GetAllSecretarias/{idPrefeitura}")]
        public List<JObject> GetAllSecretarias(Int64 idPrefeitura)
        {
            return _gs.GetAllSecretarias(idPrefeitura);
        }

        /// <summary>
        /// Salva os dados de uma prefeitura 
        /// </summary>
        /// <param name="prefeitura">Prefeitura cujos dados serão salvos</param>
        /// <returns></returns>
        [AcceptVerbs("POST"), Route("SavePrefeitura")]
        public HttpResponseMessage SavePrefeitura(Prefeitura prefeitura)
        {
            try
            {
                prefeitura = _gs.SavePrefeitura(prefeitura);
                Ok();
                return  new HttpResponseMessage(HttpStatusCode.OK)
                {
                    ReasonPhrase = "100|Dados da prefeitura salvos com suscesso!|" + prefeitura.Id
                };
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = String.Format("500|Dados não salvos. Erro:{0}-{1}", e.HResult, e.Message) 
                };
            }
        }

        /// <summary>
        /// Salva os dados de um secretaria no banco de dados 
        /// </summary>
        /// <param name="secretaria">secretaria cujos dados serão salvos </param>
        /// <returns>Secretaria</returns>
        [AcceptVerbs("POST"), Route("SaveSecretaria")]
        public HttpResponseMessage SaveSecretaria(Secretaria secretaria)
        {
            try
            {
                secretaria = _gs.SaveSecretaria(secretaria);
                Ok();
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    ReasonPhrase = "100|Dados da secretaria salvos com suscesso!|" + secretaria.Id
                };
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = String.Format("500|Dados não salvos. Erro:{0}-{1}", e.HResult, e.Message) 
                };
            }
        }
        /// <summary>
        /// Busca por um servidor pelo id repassado 
        /// </summary>
        /// <param name="id">Id do servidor</param>
        /// <returns>Servidor ou null</returns>
        [AcceptVerbs("GET"), Route("FindServidorById/{id}")]
        public Servidor FindServidorById(Int64 id)
        {
            return _gs.FidServidorById(id);
        }
        
    }
}