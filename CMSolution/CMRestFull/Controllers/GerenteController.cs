﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CMCore.Repository;
using CMCore.Service;
using CMRestFull.Class;
using Newtonsoft.Json.Linq;
using CMCore.Model;
using CMCore.Context;

namespace CMRestFull.Controllers
{
    [RoutePrefix("api/gerente")]
    public class GerenteController : ApiController
    {
        private IGerenteService _gs;

        public GerenteController(IGerenteService gs)
        {
            _gs = gs;
        }

        [AcceptVerbs("GET")]
        [Route("Logar/{login}/{senha}")]
        public JObject Logar(string login, string senha)
        {
            return _gs.Logar(login, senha);
        }

        [AcceptVerbs("GET")]
        [Route("ProtocolosByIdServidor/{idServidor}")]
        public List<Protocolo> ProtocolosByIdServidor(Int64 idServidor)
        {
            return _gs.ProtocolosByIdServidor(idServidor);
        }

        [AcceptVerbs("POST")]
        [Route("SalvarServidor")]
        public HttpResponseMessage SalvaServidor(Servidor servidor)
        {
            try
            {
                servidor = _gs.SalvaServidor(servidor);
                Ok();
                HttpResponseMessage resporta = new HttpResponseMessage(HttpStatusCode.OK);
                resporta.ReasonPhrase = "100|Gravado com sucesso!|"+ servidor.Id;
                return resporta;
            }
            catch (Exception e)
            {
                var resposta = new HttpResponseMessage(HttpStatusCode.BadRequest);
                resposta.ReasonPhrase = "500|" + e.Message;
                return resposta;
            }
        }
    }
}