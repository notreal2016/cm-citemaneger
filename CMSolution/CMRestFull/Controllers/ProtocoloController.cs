﻿using CMCore.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Results;
using CMCore.Context;
using CMCore.Model;
using CMCore.EntidadeSingle;
using Newtonsoft.Json.Linq;

namespace CMRestFull.Controllers
{
    [RoutePrefix("api/protocolo")]
    public class ProtocoloController : ApiController
    {
        private readonly IProtocoloService _service;

        public ProtocoloController(IProtocoloService service)
        {
            _service = service;
        }

        /// <summary>
        /// Busca por todos os procedimentos de um determinado protocolo repassado por parametro
        /// </summary>
        /// <param name="phone">Telefone celular vinculado ao protocolo</param>
        /// <param name="identification">Protocolo</param>
        /// <returns>Lista de procedimentos localizados</returns>
        [AcceptVerbs("GET")]
        [Route("Acompanha/{phone}/{identification}")]
        public List<Procedimento> FindProcedimentosbyProtocol(string phone, string identification)
        {
            
            return _service.FindProcedimentosbyProtocol(phone, identification);
        }

        /// <summary>
        /// Recebe um protocolo para ser persistido no banco de dados - RF2
        /// </summary>
        /// <param name="protocolo">Protocolo a ser recebido</param>
        /// <returns>Identificação do protocolo</returns>
        [AcceptVerbs("POST")]
        [Route("abrirProtocolo")]
        public HttpResponseMessage AbrirProtocolo(Protocolo protocolo)
        {
            try
            {
               protocolo = _service.Save(protocolo);
               // procedimento para o envio do SMS, e-mail e Whatsapp
                
                Ok();
                HttpResponseMessage resporta = new HttpResponseMessage(HttpStatusCode.OK);
                resporta.ReasonPhrase = protocolo.Identification;
                return resporta;
                    
            }
            catch (Exception e)
            {
                var resposta = new HttpResponseMessage(HttpStatusCode.BadRequest);
                resposta.ReasonPhrase = "500-" + e.Message;
                return resposta;
            }
            
        }
        /// <summary>
        /// Efetua o registro de uma avaliação de um protocolo
        /// </summary>
        /// <param name="identification">Identification do protocolo a ser avaliado</param>
        /// <param name="avaliacao">avaliação a ser repassada</param>
        /// <param name="comentario">Comentario do avaliador</param>
        /// <returns>Resposta da avaliação</returns>
        [AcceptVerbs("GET")]
        [Route("AvaliaProtocolo/{identification}/{avaliacao}/{comentario}")]
        public String AvaliaProtocolo(String identification, TipoAvaliacao avaliacao, String comentario)
        {
            try
            {
                String response;
                Protocolo p = _service.FindByIdentification(identification);
                if (p == null)
                {
                   
                    response = "Protocolo informado não foi localiado.";
                    return response;
                }

                if (!(p.Avaliacao == TipoAvaliacao.INSATISFEITO || p.Avaliacao == TipoAvaliacao.NAOAVALIADO))
                {
                   
                    response = "Protocolo informado já foi avaliado.";
                    return response;
                }

                p.Avaliacao = avaliacao;
                if (avaliacao == TipoAvaliacao.INSATISFEITO || p.Avaliacao == TipoAvaliacao.NAOAVALIADO)
                {
                    Procedimento pro = new Procedimento()
                    {
                        IdProtocolo = p.Id,
                        Datahora = DateTime.Now,
                        Descricao = comentario,
                        Tipo = TipoProcedimento.REABERTO
                    };
                    p.addProcedimento(pro);
                    p.Status = Status.REABERTO;
                }
                else
                {
                    p.Status = Status.CONCLUIDO;
                }

                _service.Save(p);
               
                response = avaliacao == TipoAvaliacao.INSATISFEITO?
                    "Seu posicionamento é muito importante, estamos repassando para o setor reponsável para devida correção.":
                    "Protocolo avaliado com sucesso! Obrigado por sua colaboração.";
                return response;
            }
            catch (Exception e)
            {
               
                return "500-" + e.Message;
             
            }
        }
        /// <summary>
        /// Busca por topos os problemas cadastrado viculados ao tipo repassado - RF2
        /// </summary>
        /// <param name="tipo">Tipo de problema</param>
        /// <returns>Lista de Problemas</returns>
        [AcceptVerbs("GET")]
        [Route("findProblemas/{tipo}")]
        public List<Problema> FindProblemasByTipo(TipoProblema tipo)
        {
            using (CMContext context = new CMContext())
            {
                return _service.ListProblemasByTipo(tipo, context);    
            }
            
        }
        /// <summary>
        /// Busca por um problema a partir do Id Repassado 
        /// </summary>
        /// <param name="id">Id a ser pesquisado</param>
        /// <returns>Problema localizado ou null</returns>
        [AcceptVerbs("GET")]
        [Route("findProblemaById/{id}")]
        public Problema FindProblemaById(Int64 id)
        {
            return _service.FindProblemaById(id);
        }
        /// <summary>
        /// Lista todos os pocolos ainda não notificados
        /// </summary>
        /// <param name="notifications">Protocolos que já foram notificados e devem ser despensados</param>
        /// <returns>Lista de protocolos ainda não notificados</returns>
        [AcceptVerbs("GET")]
        [Route("GetNotificationsList/{notifications}")]
        public List<ProtoTele> GetNotificationList(String notifications)
        {
            String[] notes = notifications.Trim().Split(',');
            return _service.UpNotAndFindNotNotification((notes.Length == 0) ? null: notes);
        }
        /// <summary>
        /// Busca por dados de um protocolo a partir do Identification e phone cadastrado
        /// </summary>
        /// <param name="identification">Identificação</param>
        /// <param name="phone">Phone</param>
        /// <returns>Protocolo cadastrado</returns>
        [AcceptVerbs("GET")]
        [Route("GetProtocolo/{identification}/{phone}")]
        public Protocolo GetProtocolo( string identification, string phone)
        {
            return _service.FindByIdAndPhone(identification, phone);
        }
    }
}
