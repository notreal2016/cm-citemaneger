﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CMCore.Context;
using CMCore.Interfaces;
using CMCore.Repository;
using CMCore.Service;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Integration.WebApi;

namespace CMRestFull
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings
                .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters
                .Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);

            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle ();
            container.Register<IProtocoloRepository, ProtocoloRepository>(Lifestyle.Scoped);
            container.Register<ISecretariaRepository, SecretariaRepository>(Lifestyle.Scoped);
            container.Register<IProtocoloService, ProtocoloService>(Lifestyle.Scoped);
            container.Register<IServidorRepository, ServidorRepository>(Lifestyle.Scoped);
            container.Register<IGerenteService, GerenteService>(Lifestyle.Scoped);
            container.Register<CMContext, CMContext>(Lifestyle.Scoped);
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration); //web api  
            container.Verify();
            DependencyResolver.SetResolver(
                new SimpleInjectorDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container); //web api

        }
    }
}
