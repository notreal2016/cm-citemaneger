﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Context;
using CMCore.Factory;
using CMCore.Model;
using CMCore.Repository;
using CMCore.Service;

namespace CMTest
{
    class Program
    {
        private static CMContext _context = FactoryContext.FactoryCmContext();
        private static ServidorRepository sr = new ServidorRepository();
        private static PrefeituraRepository pr = new PrefeituraRepository();
        private static SecretariaRepository sc = new SecretariaRepository();
        private static ProtocoloRepository prr = new ProtocoloRepository();
        private static ProtocoloService ps = new ProtocoloService(prr, sc);
        static void Main(string[] args)
        {

            Servidor s = null;
            CMContext c = FactoryContext.FactoryCmContext();
            s = c.Servidores.Find(1);
            using (CMContext context = new CMContext())
            {
                s.Nome = s.Nome + " 1 ";
                context.Entry(s).State = EntityState.Modified;
                context.SaveChanges();
                c.Entry(s).Reload();
            }

            Console.WriteLine(s.Nome);
            Console.ReadLine();

            /* CriandoServidores();
             CriaPrefeitua();
             CriaSecretaria();
             CriarProblema();
             CriaProtocolo();
             CriaProcedimentos();
             
             ProtocoloService ps = new ProtocoloService(new ProtocoloRepository(), new SecretariaRepository());
             var p = ps.FindByIdAndPhone("201903300001","(83) 99107-6105");
             Console.WriteLine(p.Endereco.Logradouro);
             Console.ReadLine();*/
        }

        
        public static void CriaProtocolo()
        {
            ProtocoloService ps = new ProtocoloService(new ProtocoloRepository(), new SecretariaRepository());
            Protocolo protocolo = new Protocolo()
            {
                Problema    = ps.FindProblemaById(1),
                Email = "laertonmarques@uol.com.br",
                Endereco = new Endereco() {Bairro = "Centro", Cep = "58800-280", Cidade = "Sousa", Latitude = (decimal) 63.23f, Logradouro = "R. de teste", Longitude = (decimal) 63.23f, Numero = 10, Uf = "PB"},
                Observacao = "Este modulo foi implantado por um teste",
                PontoReferencia = "Este modulo foi implantado por um teste",
                Telefone = "83991076105",
                
            };
            ps.Save(protocolo);
            Console.WriteLine(protocolo.Identification);
            Console.ReadLine();
        }

        private static void CriaProcedimentos()
        {
            List<Protocolo> protocolos = prr.FindAllbySec(1);
            protocolos.ForEach(p => Console.WriteLine("Problema :" + p.Problema.Descricao + "\n\r" +
                                                      "Secretaria Responsável :" + p.Problema.Secretaria.Nome + "\n\r" +
                                                      "Prefeitura Responsável :" + p.Problema.Secretaria.Prefeitura.Nome
            ));
            Console.Read();
            Servidor s = sc.FindById(1).Servidores[0].Servidor;
            Protocolo p1 = protocolos.First();
            p1.addProcedimento(new Procedimento() {Tipo = TipoProcedimento.ENCAMINHADO});
            p1.addProcedimento(new Procedimento() {Tipo = TipoProcedimento.EXECURCAO, Servidor = s});
            prr.Save(p1);
        }

        

        private static void CriarProblema()
        {
            List<Secretaria> secs = sc.AllSecretariasByPrefeitura(1);
            secs.ForEach(s => Console.WriteLine(s.Nome));
            Console.Read();
            Secretaria s1 = sc.FindById(1);
            Problema p1 = new Problema()
            {
                Descricao = "Lâmpada queimada.",
                TempoConclusao = 24,
                Tipo = TipoProblema.ILUMINACAO,
            };
            s1.AddProblema(p1);
            sc.Save(s1);
        }

        private static void CriaSecretaria()
        {
            Prefeitura pf = pr.FindById(1);
            Servidor s1 = sr.FindById(2);
            Servidor s2 = sr.FindById(3);
            Secretaria sc = new Secretaria()
            {
                Nome = "Saúde",
                Email = "Saude@prefeituraifpb.gov.br",
                Endereco = pf.Endereco,
                Secretario = s1,
                Horario = new Horario() { Abertura = new TimeSpan(8,00,00), Fechamento = new TimeSpan(17,0,0), Descricao = "Horario corrido"},
                Telefone = "083 3522 1822"
            };
            
            pf.AddSecretaria(sc);
            pr.Save(pf);
            sc.VinculaServidor(s2);
            pr.Save(pf);
        }

        private static void CriaPrefeitua()
        {
            Servidor s1 = sr.FindById(1);
            Prefeitura prefeitura = new Prefeitura()
            {
                Nome = "Prefeitura do IFPB",
                Endereco = new Endereco()
                {
                    Logradouro = "Rua projetada", Bairro = "Centro", Cep = "58900-000", Cidade = "Cajazeiras", Numero = 100,
                    Uf = "PC"
                },
                Prefeito = s1
            };
            pr.Save(prefeitura);
            Console.WriteLine(prefeitura.Id + " - " + prefeitura.Nome);
            Console.Read();
        }

        private static void CriandoServidores()
        {
            Servidor s0 = new Servidor()
            {
                Nome = "Laerton Marques de Figueiredo",
                Celular = "083991076105",
                Email = "laerton240311@gmail.com",
                Login = "laerton",
                Senha = "senha1",
                Matricula = "09999",
                Tipo = TipoServidor.PREFEITO
            };
            Servidor s1 = new Servidor()
            {
                Nome = "Fulando de tal",
                Celular = "083991076107",
                Email = "sicarano@gmail.com.br",
                Login = "laerton",
                Senha = "senha1",
                Matricula = "09992",
                Tipo = TipoServidor.FUNCIONARIO
            };

            Servidor s3 = new Servidor()
            {
                Nome = "Sicrano",
                Celular = "083991076102",
                Email = "sicarano1@gmail.com.br",
                Login = "laerton",
                Senha = "senha1",
                Matricula = "09995",
                Tipo = TipoServidor.SECRETARIO
            };

            
            s0 = sr.Save(s0);
            s1 = sr.Save(s1);
            s3 = sr.Save(s3);
            Console.WriteLine(s0.Id + ", " + s1.Id + ", " + s3.Id);
            Console.Read();
        }
    }
}
