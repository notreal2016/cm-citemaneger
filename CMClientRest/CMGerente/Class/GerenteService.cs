﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMCore.EntidadeSingle;
using CMCore.Model;
using CMLibClientRest;
using CMRestFull.Class;
using Newtonsoft.Json.Linq;

namespace CMGerente.Class
{
    public class GerenteService
    {
        private static readonly String ENDERECO = @"http://localhost:50947";//@"http://200.98.146.114:82";
        private static ClientProtocolo _cp = new ClientProtocolo(ENDERECO);

        /// <summary>
        ///Busca por todas as secretarias de um prefeitura pelo id repassado
        /// </summary>
        /// <param name="idPrefeitura">Id da prefeitura</param>
        /// <returns>Lista de todas as secretarias encontradas id e nome</returns>
        public static List<JObject> GetAllSecByPrefeitura(Int64 idPrefeitura)
        {
            return null;
            //return _cp.GetAllSecretarias(idPrefeitura);
        }

        /// <summary>
        /// Busca por um servidor apartir do id repassado
        /// </summary>
        /// <param name="id">id do servidor</param>
        /// <returns>Servdor localizado ou null</returns>
        public static Servidor FindServidorById(long? id)
        {
            return null;
            //return _cp.FindServidorById(id);
        }

        /// <summary>
        /// Efetua o login de um servidor no sistema
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="senha">Senha</param>
        /// <returns>Servidor</returns>
        public static JObject Logar(String login, String senha)
        {
            return _cp.Login(login, senha);
        }
        /// <summary>
        /// Vinculaum servidor a uma secretaria
        /// </summary>
        /// <param name="idServidor">Id do servidor</param>
        /// <param name="idSecretaria">Id da secretaria</param>
        /// <returns>Mensagme de retorno</returns>
        public static String VinculaServidor(Int64 idServidor, Int64 idSecretaria)
        {
            //return _cp.VinculaServidor(idServidor, idSecretaria);
            return null;
        }
        /// <summary>
        /// Salva os dados de um servidor no sistema 
        /// </summary>
        /// <param name="servidor">Servidor cujos dados serão salvos</param>
        /// <returns>Dados Salvos</returns>
        public static Servidor SaveServidor(Servidor servidor)
        {
            
            string[] resp = _cp.SaveServidor(servidor).Split('|');
            if (resp[0].Equals("500"))
            {
                throw  new Exception(resp[0] + "-" + resp[1]);
            }

            if (servidor.Id == 0)
            {
                servidor.Id = Convert.ToInt64(resp[2]);
            }
            return servidor;
            
        }

        /// <summary>
        /// Busca por todos os protocolos das secretarias vinculadas ao id do servidor
        /// </summary>
        /// <param name="servidorId">id do servidor</param>
        /// <returns>Lista de protocolos</returns>
        public static List<Protocolo> FindProtocolosByServidor(long servidorId)
        {
            return _cp.Protocolos(servidorId);
        }
    }
}