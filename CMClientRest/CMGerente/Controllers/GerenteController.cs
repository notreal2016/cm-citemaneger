﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using CMCore.Model;
using CMGerente.Class;
using CMLibClientRest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SelectListItem = System.Web.WebPages.Html.SelectListItem;

namespace CMGerente.Controllers
{
    public class GerenteController : Controller
    {
        private static string _mensagem = String.Empty;
        private static Servidor _editServidor = null;
        private static Servidor _servidor = null; 
        private static List<Protocolo> _protocolos = new List<Protocolo>();
        // GET: Gerente

        public ActionResult Logar(FormCollection dados)
        {
            JObject user = GerenteService.Logar(dados["login"], dados["senha"]);
            int cod = Convert.ToInt32( user.GetValue("Cod").ToString());
            if (cod >= 500)
            {
                _mensagem = user.GetValue("Mensagem").ToString();
                return RedirectToAction("Erro", "Gerente");
            }
            Session["user"] = user;
            _servidor = JsonConvert.DeserializeObject<Servidor>(user.GetValue("Servidor").ToString());
            return RedirectToAction("Index", "Gerente");
        }

        public ActionResult Index()
        {

            _protocolos = GerenteService.FindProtocolosByServidor(_servidor.Id);
            List<JObject> posicoes = new List<JObject>();
            string result = "[";
            int i = 0;
            _protocolos.OrderBy(p1 => p1.Datahora).ToList().ForEach(p =>
            {
                String posicao = String.Format("latitude: {0}, longitude: {1}, icon:{2}, data:{3}, prazo:{4}, fora:{5}",
                    p.Endereco.Latitude.ToString(new CultureInfo("en-US")),p.Endereco.Longitude.ToString(new CultureInfo("en-US")), (int) p.Problema.Tipo,
                    p.Datahora.ToShortDateString(), p.Problema.TempoConclusao, p.Datahora.AddHours(p.Problema.TempoConclusao)< DateTime.Now?1:0);
                result += "{" + posicao + "}";
                if (i < _protocolos.Count - 1)
                {
                    result += ",";
                    i++;
                }
            });
            result += "]";
            ViewBag.posicoes = result;
            ViewData["protocolos"] = _protocolos;
            return View();
        }

        public ActionResult Erro()
        {
            ViewBag.Mensagem = _mensagem;
            return View();
        }

        public ActionResult LogOff()
        {
            Session.Abandon();
            return RedirectToAction("login", "Home");
        }

        public ActionResult NovoServidor()
        {
            _editServidor = new Servidor();
            return RedirectToAction("Servidor", "Gerente");
        }

        public ActionResult Servidor()
        {
            ViewData["protocolos"] = _protocolos;
            ViewData["ListaTipos"] = new SelectList(Enum.GetValues(typeof(TipoServidor)).Cast<TipoServidor>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value =((int)v).ToString(),
                Selected = (_editServidor != null && _editServidor.Tipo == v)
            }).ToList(),"Value", "Text");
            return View(_editServidor);
        }

        public PartialViewResult dadosServidor()
        {
            ViewData["ListaTipos"] = new SelectList(Enum.GetValues(typeof(TipoServidor)).Cast<TipoServidor>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value =((int)v).ToString(),
                Selected = (_editServidor != null && _editServidor.Tipo == v)
            }).ToList(),"Value", "Text");
            return PartialView();
        }

        public ActionResult SaveServidor(FormCollection dados)
        {
            Console.WriteLine(dados["id"]);
            if (dados["id"].Equals("0"))
            {
                _editServidor = new Servidor();
            }

            _editServidor.Nome = dados["nome"];
            _editServidor.Matricula = dados["Matricula"];
            _editServidor.Tipo = (TipoServidor) int.Parse(dados["Matricula"]);
            _editServidor.Celular = dados["celular"];
            _editServidor.Email = dados["email"];
            _editServidor.Login = dados["login"];
            _editServidor.Senha =
                ((dados["senha"] != null && !dados["senha"].IsEmpty()) || Convert.ToInt64(dados["id"]) == _servidor.Id)
                    ? dados["senha"]
                    : _editServidor.Senha;
            _editServidor = GerenteService.SaveServidor(_editServidor);
            return RedirectToAction("Servidor", "Gerente");
        }
    }
}