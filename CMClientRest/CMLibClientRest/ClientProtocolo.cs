﻿using CMCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using CMRestFull.Class;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CMLibClientRest
{
    /// <summary>
    /// Classe encarregada das ações rest para os processo de pesquisa e cadastro de protocolo
    /// </summary>
    public class ClientProtocolo
    {
        private String url = string.Empty;
        private string contentType = "application/json; charset=utf-8";
        private WebRequest webRequest;


        public ClientProtocolo(string url)
        {
            this.url = url;
        }

        /// <summary>
        /// Busca por todos os probelmas relacionados tipo especifico
        /// </summary>
        /// <param name="tipo">Tipo de problema relacionado</param>
        /// <returns>Lista de problemas encontrados caso não encontre retorna uma lista vazia</returns>
        public List<Problema> FindAllProblemasByType(TipoProblema tipo)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    string comple = url + @"/api/protocolo/findProblemas/" + (int)tipo;
                    var result = client.GetStringAsync(comple).Result;
                    var post = JsonConvert.DeserializeObject<List<Problema>>(result);
                    return post.ToList();
                }
                catch (Exception e)
                {
                    throw new Exception("Erro no sistema, contate suporte e repasse as informações a baixo \nErro: " + e.HResult + " - " + e.Message);
                }
            }
            
        }
        /// <summary>
        /// Valida o login de um servidor no sistema
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="senha">Senha</param>
        /// <returns>Dados de login</returns>
        public JObject Login(string login, string senha)
        {
            JObject result = new JObject();
            if (ValidaDados.ExistCaracterEspeciais(login) ||senha == null || senha.Trim().Equals(""))
            {
                result.Add("Cod", 500);
                result.Add("Mensagem", "Login ou senha incorretos.");
                return result;
            }

            using (var client = new HttpClient())
            {
                
                try
                {
                    string comple = url + string.Format(@"/api/gerente/Logar/{0}/{1}", login, senha);
                    var dados = client.GetStringAsync(comple).Result;
                    var post = JsonConvert.DeserializeObject<JObject>(dados);
                    return post;
                }
                catch (Exception e)
                {
                    result.Add("Cod", 505);
                    result.Add("Mensagem", e.HResult + " - " + e.Message);
                    return result;
                }
            }
            
        }
        /// <summary>
        /// Busca por um protocolo apartir do identification
        /// </summary>
        /// <param name="identification">nº do protocolo</param>
        /// <param name="phone">telefone cadastrado para o protocolo</param>
        /// <returns></returns>
        public Protocolo GetProtocolo(string identification, string phone)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    string comple = url + @"/api/protocolo/getProtocolo/" + identification + "/" + phone;
                    var result = client.GetStringAsync(comple).Result;
                    var post = JsonConvert.DeserializeObject<Protocolo>(result);
                    return post;
                }
                catch (Exception e)
                {
                    throw new Exception("Erro no sistema, contate suporte e repasse as informações a baixo \nErro: " + e.HResult + " - " + e.Message);
                }
            }
        }

        

        /// <summary>
        /// Busca por todas as etapas de um determindo protocolo
        /// </summary>
        /// <param name="phone">Telefone</param>
        /// <param name="identification">Numero do protocolo</param>
        /// <returns>Lista de procedimentos</returns>
        public List<Procedimento> Procedimentos(String phone, string identification)
        {
            using (var client = new HttpClient())
            {
                string result = "";
                try
                {
                    string comple = url + String.Format(@"/api/protocolo/acompanha/{0}/{1}", phone, identification);
                    result = client.GetStringAsync(comple).Result;
                    
                    if (!result.Equals("[null]") )
                    {
                        var post = JsonConvert.DeserializeObject<List<Procedimento>>(result);
                        return post.ToList();
                    }

                    return null;

                }
                catch (Exception e)
                {
                    var post = new JObject(result);
                    String men = post["ExceptionMessage"].ToString();             
                    throw new Exception(men);
                }
            }
        }
        /// <summary>
        /// Busca por um tipo de problema a partir do Id repassado
        /// </summary>
        /// <param name="id">Id do problema a ser buscado</param>
        /// <returns>Problema localizado ou null</returns>
        public Problema FindyByIdProblema(Int64 id)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    string comple = url + @"/api/protocolo/findProblemaById/" + id;
                    var result = client.GetStringAsync(comple).Result;
                    var post = JsonConvert.DeserializeObject<Problema>(result);
                    return post;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        private void PreparaWebRequest(string operacao, string metodo)
        {

            webRequest = System.Net.WebRequest.Create(url + "/" + operacao);
            webRequest.Credentials = CredentialCache.DefaultCredentials;
            webRequest.Method = metodo;
            webRequest.ContentType = this.contentType;
            //Pendente para resolver o post
        }
        /// <summary>
        /// Abri um novo protocolo
        /// </summary>
        /// <param name="protocolo">Protocolo a ser enviado ao serviço web</param>
        /// <returns>String contendo a chava Identification ou mensagem de erro do serviço web</returns>
        public String AbrirProtocolo(Protocolo protocolo)
        {
            try
                {
                    string comple = @"/api/protocolo/abrirProtocolo/";
                    PreparaWebRequest(comple,"POST");
                    var obj = JsonConvert.SerializeObject(protocolo);
                    var data = Encoding.UTF8.GetBytes(obj);
                    using (var stream = webRequest.GetRequestStreamAsync().Result)
                    {
                        stream.Write(data,0,data.Length);
                    }
                    WebResponse response = webRequest.GetResponseAsync().Result;
                    return ((HttpWebResponse)response).StatusDescription;
                }
                catch (Exception e)
                {
                    throw new Exception (e.Message);
                }
         }
        /// <summary>
        /// Encarregado de enviar os dados de uma avaliação ao serviço web
        /// </summary>
        /// <param name="identification">Chave de identificação unica do protocolo</param>
        /// <param name="tipoAvaliacao">Tipo de avaliação</param>
        /// <param name="comentario">Comentário repassado, como defalut é em branco</param>
        /// <returns></returns>
        public  String AvaliaProtocolo (String identification, int tipoAvaliacao, String comentario = "")
        {
            string comple = url + string.Format(@"/api/protocolo/AvaliaProtocolo/{0}/{1}/{2}", identification, tipoAvaliacao,
                comentario);
            using (var client = new HttpClient())
            {
                try
                {
                    var result = client.GetStringAsync(comple).Result;
                    var post = JsonConvert.DeserializeObject<String>(result);
                    return post;
                }
                catch (Exception e)
                {
                    return "500-" + e.Message;
                }
            }
        }

        /// <summary>
        /// Busca por todos os protocolos vinculados as secretarias que o id do funcionário tem registro
        /// </summary>
        /// <param name="servidorId">Id do servidor</param>
        /// <returns>Lista de protocolos</returns>
        public List<Protocolo> Protocolos(long servidorId)
        {
            string comple = url + string.Format(@"/api/gerente/ProtocolosByIdServidor/{0}", servidorId);
            using (var client = new HttpClient())
            {
                try
                {
                    var result = client.GetStringAsync(comple).Result;
                    var post = JsonConvert.DeserializeObject<List<Protocolo>>(result);
                    return post;
                }
                catch (Exception e)
                {
                    throw  new Exception("500 - " + e.Message);
                }
            }
        }

        public string SaveServidor(Servidor servidor)
        {
            try
            {
                string comple = @"/api/gerente/SalvarServidor/";
                PreparaWebRequest(comple,"POST");
                var obj = JsonConvert.SerializeObject(servidor);
                var data = Encoding.UTF8.GetBytes(obj);
                using (var stream = webRequest.GetRequestStreamAsync().Result)
                {
                    stream.Write(data,0,data.Length);
                }
                WebResponse response = webRequest.GetResponseAsync().Result;
                return ((HttpWebResponse)response).StatusDescription;
            }
            catch (Exception e)
            {
                throw new Exception (e.Message);
            }
        }
    }
    

}
