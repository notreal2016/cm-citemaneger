﻿using CMLibClientRest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMCore.Model;

namespace Protocolo.Service
{
    /// <summary>
    /// Classe encarregada das funções de serviço do protocolo
    /// </summary>
    public class ServiceProtocolo
    {
        private static readonly string _url = "http://localhost:50947";
        private static ClientProtocolo cp = new ClientProtocolo(_url);
        /// <summary>
        /// Busca por procedimentos realizados em um protocolo
        /// </summary>
        /// <param name="phone">Telefone celular</param>
        /// <param name="identification">Numero do protocolo</param>
        /// <returns>Lista de procedimentoss</returns>
        public static List<Procedimento> Procedimentos(string phone, string identification)
        {
            return cp.Procedimentos(phone, identification);
        }

        public static CMCore.Model.Protocolo GetProtocolo(string phone, string identification)
        {
            return cp.GetProtocolo(identification, phone);
        }
        /// <summary>
        /// Envia uma avaliação de um protocolo pelo identification repassado 
        /// </summary>
        /// <param name="identification">Identificação do protocolo</param>
        /// <param name="tipoAvaliacao">Tipo de avaliação</param>
        /// <param name="comentario">Comentário da avaliação como default é em branco</param>
        /// <returns>Mensagme de resposta </returns>
        public static String AvaliaProtocolo(string identification, int tipoAvaliacao, string comentario = "")
        {
            return cp.AvaliaProtocolo(identification, tipoAvaliacao, comentario);
        }
    }
}