﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMCore.Model;
using Protocolo.Service;


namespace Protocolo.Controllers
{
    public class AcompanhaController : Controller
    {
        // GET: Acompanha
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Etapas(FormCollection dados)
        {
            try
            {
                List<Procedimento> procedimentos = ServiceProtocolo.Procedimentos(dados["telefone"], dados["protocolo"]);
                CMCore.Model.Protocolo protocolo = ServiceProtocolo.GetProtocolo(dados["telefone"], dados["protocolo"]);
                ViewData["procedimentos"] = procedimentos;
                ViewData["protocolo"] = protocolo;

                return View();
            }
            catch (Exception e)
            {
                return RedirectToAction("ErroProtocolo", "Acompanha") ;
            }
        }
        public ActionResult ErroProtocolo()
        {
            return View();
        }

        public ActionResult RespostaAvaliacao(FormCollection dados)
        {
         
            try
            {
                int tipoAvaliacao = int.Parse(dados["Tipo"]);
                var resposta =
                    ServiceProtocolo.AvaliaProtocolo(dados["identification"], tipoAvaliacao , dados["comentarios"] == null? "": dados["comentarios"]);
                ViewBag.resposta = resposta;
                return View();
            }
            catch (Exception e)
            {
                ViewBag.resposta = "Você não selecionou umas das opções de avaliações.";
                return View();
            }


        
        }
    }

    
}