﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMCore.Model;
using CMLibClientRest;
using WebGrease.Css.Extensions;
using Protocolo = CMCore.Model.Protocolo;

namespace ProtocoloApp.Controllers
{
    public class HomeController : Controller
    {
        
        private static Random rando = new Random();
        private static readonly string _url = "http://localhost:50947";
        public static string MErro { get; set; }
        public static int chaveAntiga;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Abrir()
        {
            return View();
        }

        public ActionResult SelecaoProtocolo(int? id)
        {
            if (id != null)
            {
                MontaRotulo(id);
            }
            ClientProtocolo cp = new ClientProtocolo(_url);

            TipoProblema tipo = (TipoProblema) Enum.Parse(typeof(TipoProblema), id.ToString());
            try
            {
                List<Problema> listaProblemas = cp.FindAllProblemasByType(tipo);
                ViewData["ListaProtocolos"] = new SelectList( listaProblemas, 
                    "Id",
                    ""
                );
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                MErro = e.Message;
                return RedirectToAction("Erro", "Home");
            }
            
            
            
            return View();
        }

        public ActionResult Erro()
        {
            ViewBag.Erro = MErro;
            return View();
        }

        public ActionResult MapAddress()
        {
            return View();
        }

        private void MontaRotulo(int? id)
        {
            switch (id)
            {
                case 1:
                    ViewBag.Img = "/Img/poste.png";
                    ViewBag.Titulo = "Iluminação";
                    break;
                case 2:
                    ViewBag.Img = "/Img/Saneamento.png";
                    ViewBag.Titulo = "Saneamento";
                    break;
                case 3:
                    ViewBag.Img = "/Img/Saude.png";
                    ViewBag.Titulo = "Saúde";
                    break;
                case 4:
                    ViewBag.Img = "/Img/Transito.png";
                    ViewBag.Titulo = "Trânsito";
                    break;
                case 5:
                    ViewBag.Img = "/Img/Educao.png";
                    ViewBag.Titulo = "Educação";
                    break;
                case 6:
                    ViewBag.Img = "/Img/Mais.png";
                    ViewBag.Titulo = "Outros";
                    break;
            }
        }

        public PartialViewResult ColetaEndereco()
        {
            ViewBag.resposta = false;
            return PartialView();
        }
      
        public PartialViewResult ColetaPosicao()
        {
            ViewBag.resposta = true;
            return PartialView();
        }

        public PartialViewResult ViewMap()
        {
            return PartialView();
        }
        

        public ActionResult confirmaProtocolo(FormCollection dados)
        {

            //Vrifica se já existe uma chave já criada
            if (Session["Chave"] == null || int.Parse(Session["Chave"].ToString()) == 0)
            {
                CriarProtocolo(dados);
                CriarChave();
            }
            else
            {
                if (chaveAntiga != 0 && chaveAntiga != int.Parse(Session["Chave"].ToString()))
                {
                    ViewBag.mensagem = "Nova chave enviada!";
                    chaveAntiga = 0;
                }
                else
                {
                    ViewBag.mensagem = "Chave inválida!";
                }
                
            }
            
            return View();
        }


        public ActionResult NovaChave()
        {
            chaveAntiga = int.Parse(Session["Chave"].ToString());
            CriarChave();
            return RedirectToAction("confirmaProtocolo","Home");
        }

        private void CriarChave()
        {
            int chave = rando.Next(250000, 999999);
            Session["Chave"] = chave;
            ViewBag.mensagem = String.Empty;
        }

        private void CriarProtocolo(FormCollection dados)
        {
            ClientProtocolo cp = new ClientProtocolo(_url);
            Problema problema =  cp.FindyByIdProblema(Int64.Parse(dados["Lista"]));
            string lat = dados["latitude"], logi = dados["longitude"];
            
            Endereco endereco = new Endereco()
            {
                Bairro = dados["Bairro"],
                Latitude = (dados["latitude"]== null)?0:Convert.ToDecimal(lat, new CultureInfo("en-US")),
                Longitude = (dados["longitude"]==null)?0:Convert.ToDecimal(logi,  new CultureInfo("en-US")),
                Logradouro = ((dados["Endereco"]!= null)?dados["Endereco"]: "-"),
        
            };
            CMCore.Model.Protocolo protocolo = new CMCore.Model.Protocolo()
            {
                
                Endereco = endereco,
                Email = dados["email"],
                Datahora = DateTime.Now,
                IdProblema = problema.Id,
                PontoReferencia = (dados["referencia"] !=null)?dados["referencia"]:"",
                Telefone = dados["telefone"],
                Observacao = dados["textarea2"],
                
               
                
            };
            protocolo.PrevConclusao = protocolo.Datahora.AddHours(problema.TempoConclusao);
            Session["NewProtocolo"] = protocolo;

        }

        public ActionResult FimProtocolo(FormCollection dados)
        {
            try
            {
                if ((Request.Form["ContraChave"] == null || Request.Form["ContraChave"] == string.Empty))
                {
                    return RedirectToAction("confirmaProtocolo", "Home");
                }
                int contraChave = int.Parse(Request.Form["ContraChave"]);
                int chave = int.Parse(Session["Chave"].ToString());
                if (chave == contraChave)
                {
                    ViewBag.NumProtocolo = GravarProtocolo();
                    Session.Abandon();
                    return View();
                }
                return RedirectToAction("confirmaProtocolo", "Home");
            }
            catch (Exception e)
            {
                MErro = e.Message;
                return RedirectToAction("Erro", "Home");
            }
            
        }

        private string GravarProtocolo()
        {
            CMCore.Model.Protocolo p = (CMCore.Model.Protocolo) Session["NewProtocolo"];
            ClientProtocolo cp = new ClientProtocolo(_url);
            return cp.AbrirProtocolo(p);
        }

        [HttpPost]
        public ActionResult SelecaoProtocolo(String protocolo)
        {
            ClientProtocolo cp = new ClientProtocolo(_url);

            TipoProblema tipo = (TipoProblema) Enum.Parse(typeof(TipoProblema), protocolo);
            List<Problema> listaTeste = cp.FindAllProblemasByType(tipo);
            
            ViewBag.ListaProtocolos = new SelectList(
                listaTeste, 
                "Protocolo",
                protocolo
            );
            return View();
        }

        public String ImagemCampo(String caminho)
        {
            return caminho;
        }
    }
}