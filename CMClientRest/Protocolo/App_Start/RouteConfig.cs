﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Protocolo
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Menu",
                url: "{controller}/{action}/{id}",
                defaults: new {controlle = "Home", action = "SelecaoProtocolo", id = UrlParameter.Optional});

            routes.MapRoute(
                name: "fimP",
                url: "{controller}/{action}/{id}",
                defaults: new {controlle = "Home", action = "FimProtocolo", id = UrlParameter.Optional});

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
