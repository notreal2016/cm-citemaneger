﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMCore.Model;
using CMLibClientRest;

namespace CMtest
{
    class Program
    {
        static void Main(string[] args)
        { 
            CMLibClientRest.ClientProtocolo protocolo = new ClientProtocolo("http://localhost:50947");
            var result = protocolo.FindAllProblemasByType(TipoProblema.ILUMINACAO);
            result.ForEach(r => Console.WriteLine(r.Descricao));
            Console.ReadLine();
        }
    }
}
