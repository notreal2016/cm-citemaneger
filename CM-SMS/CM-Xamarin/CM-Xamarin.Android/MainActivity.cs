﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Provider;
using Android.Telephony;
using Android.Content;

namespace CM_Xamarin.Droid
{

    [Activity(Label = "CM_Xamarin", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
            SmsManager sms = SmsManager.Default;
            PendingIntent sentPI;
            String SENT = "SMS_SENT";
            sentPI = PendingIntent.GetBroadcast(this, 0, new Intent(SENT), 0);
            sms.SendTextMessage("083999587341", null, "Mensagem de teste do App de Laaerton.", sentPI, null);
        }
    }
}

